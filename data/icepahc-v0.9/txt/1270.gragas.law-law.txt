1. UM LÖGMÆT FRUMHLAUP .
Fjörbaugsgarður varðar ef maður hleypur til manns lögmætu frumhlaupi .
En þau eru fimm hlaup , ef maður höggur til manns eða drepur eða leggur eða skýtur eða kastar .
Þá varða öll fjörbaugsgarð ef eigi kemur á , en skóggang ef á kemur .
En þá er frumhlaup lögmætt ef maður reiðir fram þann vígvöl er hann vill öðrum mein með gera ,
og væri hann svo nær að hann mundi taka til hins ef hann hæfði , eða ekki stöðvaði á gangi .
Það er enn frumhlaup , hverju sem hann skýtur eða verpur , ef hann væri svo nær að hann mundi því koma til þess ef hann hæfði eða ekki stöðvaði .
2. UM ÞAÐ EF FRUMHLAUP ER Á GANGI STÖÐVAÐ .
Þá er frumhlaup á gangi stöðvað ef kemur á vopn eða voðir eða á völl , eða taki menn við á lofti .
Ef menn þræta um frumhlaup , hvort á hafi komið eða eigi ,
og skal því að einu drep vera ef kviður ber það að á hinn mundi koma ef hann stæði ber fyrir .
En um þau frumhlaup fimm , er nú eru talið og fjörbaugsgarð varða , þá er eigi sókn til nema þau sé lýst fyrir búum fimm fyrir þriðju sól , eða nefndir að sýnarvottar ella á hinum sama vettvangi .
3. UM ÞAÐ FRUMHLAUP ER MAÐUR FELLIR MANN .
Það er hið sétta frumhlaup ef maður fellir mann ,
og varðar það skóggang .
En það er fall ef hinn styður niður kné eða hendi , allra helst ef hann fellur meir .
En það er hið sjöunda frumhlaup , ef maður rænir mann handráni .
En það er handrán ef maður slítur úr höndum manni það er hinn heldur á áður , eða af baki honum .
Það varðar og skóggang .
En það er hið áttunda ef maður ryskir mann ,
og varðar það skóggang .
Það er hið níunda ef hann kyrkir mann ,
og varðar það skóggang .
Um frumhlaup þau níu , sem þar eru nú talið , þar er vígt í gegn þeim öllum á hinum sama vettvangi ,
og eigi lengur en svo .
4. UM HANDRÁN OG FELLING OG RYSKING OG KYRKING .
Það er mælt um þau frumhlaup fjögur er skóggang varða , felling og handrán , rysking og kyrking , að þau er eigi skylt að lýsa ef þau fara ein saman , og gerist ekki fleira af á þeim vettvangi .
En þó skal búa kveðja heiman til .
Nú gerist fleira af á þeim vettvangi .
Þá skal þau lýsa sem önnur frumhlaup , og mest til óhelgi þeim manni er hljóp fyrir öndverðu nokkuru því frumhlaupi .
5. UM ÞAÐ FRUMHLAUP ER Á UNNIN VERK VERÐA .
Ef maður hleypur lögmætu frumhlaupi til manns á þeim vettvangi er á unnin verk verða með mönnum af því síðan ,
og varðar það skóggang þótt það varði fjörbaugsgarð ef það færi eitt saman .
Það er enn , ef maður hleypur lögmætu frumhlaupi til manns ,
og hleypur hann til óhelgi sér og sínum lagsmönnum , þeim er vissu með honum að hann mundi því hlaupi hlaupa á þeim vettvangi , nema hann hlaupi til þess manns er áður hefir til óhelgi sér verkað .
Nú hleypur maður í gegn úr öðrum flokki ,
og hleypur sá til þess manns er eigi vissi með honum ráð ,
og verða þá frumhlaup í báða flokka .
Ef menn verða vegnir eða sárir í hvorntveggja flokk , og verða alls sárir þrír eða fleiri , þá skal í þann flokk frumhlaup bera er fyrri var úr hlaupið ,
en hvorirtveggju eru ælir til dóms .
6. UM SÁR .
Ef maður særir mann ,
og varðar það skóggang .
En það er sár ef þar blæðir sem á kom .
En þó að maður ljósti mann á meðal herða eða á nasir , svo að blóð hrjóti úr munni eða nösum ,
og er það eigi sár ef eigi blæðir þar sem á kom .
7. UM VÍG .
Ef maður vegur mann ,
og varðar það skóggang .
Sá maður er óæll til dóms er hann vegur mann eða veitir hin meiri sár .
En þau eru hin meiri sár , heilund og holund og mergund .
Það er heilund er rauf er á hausi til heila , hvort sem hann er höggvinn eða rifnaður eða brotinn .
En þá er holund ef blóð má falla á hol úr sári .
En þá er mergund ef bein er í sundur til mergjar , það sem mergur er í , hvort sem það er höggvið eða brotið .
Það mest sem hin meiri sár ef maður sker tungu úr höfði manni eða stingur augu úr höfði eða sker af manni nef eða eyru eða brýtur tenn úr höfði manni .
En þá er skorið er skeður beini eða brjóski .
Svo er og ef maður geldir mann eða höggur klámhögg um þjó þver .
Ef maður lýstur mann svo að bein brotni , og mest það sem hin meiri sár þótt eigi blæði , með hverigu sem maður lýstur mann ,
og varðar það skóggang .
Slíkt skal og allt hið sama þótt hann veiti hinum af öðrum hlutum beinbrot .
8. UM HEIMANFARAR TIL ÁVERKA .
Hvar þess er menn fara með þann hug heiman að þeir vilja á mönnum vinna ,
og varðar skóggang ef fram kemur en fjörbaugsgarð ella .
Þeir verða og óhelgir við áverkum öllum , er fyrri koma á för til þess fundar er þeir hittast á , þótt hinir hlaupi fyrri er síðar fóru heiman .
9. EF MAÐUR STENDUR FYRIR VEGANDA .
Ef maður stendur fyrir þeim manni er mann hefir vegið eða á manni hefir unnið eða veitir hann honum lið á þeim vettvangi ,
og varðar það skóggang .
Hann verður þá óheilagur fyrir öllum áverkum á þeim vettvangi við þá menn alla er hins vilja hefna þar er á er unnið , nema hann vildi skilja þá réttum skilnaði , og verði hinum það að liði er á hefir unnið ,
og varðar honum ekki sú veisla .
En þá skill hann þá réttum skilnaði ef hann getur þann kvið að hann mundi svo skilja þá , þótt sá hefði slíkan svo áverka á sér fengið er nú hefir öðrum veitt , sem hinn hefir er þá er fyrir orðinn .
Þótt eigi verði fleiri menn vegnir eða sárir eða lostnir , svo að blátt eða rautt sé eftir , en tveir , og sé úr sínum flokki hvor , þá eru hvorirtveggju ælir til dóms ef þeir bregðast frumhlaupum við , hverngan veg er um frumhlaup berst .
Nýmæli :
Ef menn forða fjörvi frumhlaupsmanns þess er áverk lét fylgja svo að þeir veri hann oddi eða eggju ,
og varðar það fjörbaugsgarð ef eigi er á vettvangi .
Nú hleypur áverkamaður í einstigi ,
og standa menn fyrir honum þar .
Þá eigu þeir er eftir sækja að beiða hina frágöngu eða framsölu að honum .
Þá varðar svo fyrirstaða sem nú var tínt , ef nokkur maður sækir sá eftir er sín á að hefna þar að lögum , eða annars manns ,
en þeygi verða þeir óhelgir fyrir áverkum er fyrir standa , ef eigi er á vettvangi .
Um þau áverk varðar fyrirstaða fyrir utan vettvang , er lýst verða annaðtveggja áður eða síðan , svo að bjargir varða .
Um þá fyrirstöðu alla er eigi er á vettvangi ,
og eru það allt stefnusakir ,
og skal kveðja til níu búa á þingi þaðan frá er sök gerðist .
10. UM DREP ÞAÐ ER BEIN BROTNA .
Ef maður drepur mann svo að bein brotna , og mest sem hin meiri sár , hvargi er maður drepur mann ,
og varðar það skóggang .
Það er drep ef maður lýstur annan með öxarhamri , og svo hverngi vígvöl er maður hefir .
Jafnt er drep þótt maður leggi til eða kasti , ef á kemur .
Slíkt er þótt klæði verði á milli , brynja eða hjálmur , ef á myndi koma ef eigi yrði það fyrir .
Drep er og þótt maður spyrni fæti á öðrum , eða hvoti hnefa .
Drep er og þótt maður reiði þann vígvöl frá manni er hann veit að þá mundi hlaupa að honum sjálft ef hann lætur laust .
Svo er og ef hann fellir á mann það er hann fær högg af .
11. UM DREP ÞAÐ ER EIGI VERÐUR ÁSÝNT .
Þrjú eru drep
og varða öll skóggang ,
og skal sækja við tylftarkvið .
Það er eitt ef svo lítt kemur á að eigi verður ásýnt eftir .
Þess dreps skal hefna á hinum sama vettvangi , og eigi lengur en svo ,
og skal lýsa ef fleira gerist af , sem felling eða kyrking ,
og verður sá þá óheilagur er drap .
Svo skal sækja um drep þetta að maður skal stefna um það að hann hafi drepið hann og hlaupið til hans lögmætu frumhlaupi , og láta varða skóggang .
Eigi má tvær sakir úr því drepi gera þótt tvennum sé lýst , en sækja um frumhlaup eitt til fjörbaugsgarðs , ef vill , og kveðja búa heiman .
Nú vænist sá maður því er drap , að hinum yrði ásýnt þar er eigi varð .
Þá verður hann svo óheilagur fyrir áverkum sem ásýnt yrði .
Það er drep annað er áverk heitir , ef maður lýstur mann svo að blátt eða rautt verður eftir , eða þrútnar hörund eða stökkur undan hold eða hrýtur blóð úr munni eða úr nösum eða undan nöglum .
Þess dreps á maður að hefna jafnlengi sem sára , og svo þeir menn er honum fylgja til ,
og er rétt að aðrir menn hefni til jafnlengdar annars dægurs þótt þeir fari eigi sjálfir til .
Slíkt er þótt hnefa sé lostið eða spyrnt , ef ásýnt verður .
12. UM ÞAÐ DREP ER HEYRN EÐA SÝN SPILLIST .
Það er og áverkadrep ef heyrn eða sýn meiðist af .
Svo er og ef maður er lostinn í öngvit fyrir bringspölum eða á böll þótt ekki verði ásýnt ,
og svo er jafnan ef maður er lostinn í svima .
Því drepi skal stefna svo að öllu sem hinu áður , nema þá skal telja rétt úr fé hans .
Þar er kostur að lýsa sér hvort , frumhlaup og drep ,
og verða þá tvær sakir ,
og á skal kveða hver áverk þau hafa fylgt drepinu er nú voru tínd .
En ef í einni lýsingu er haft bæði drep og frumhlaup , eða annað verður lýst eða bæði ólýst , þá er ein sök .
Það er drep hið þriðja ef bein brotna ,
og verður sá óæll til dóms er drepið hefir
og á eigi þingreitt .
Svo skal sækja sem um sár hin meiri um björg , ef lýst verður ,
og skal á kveða ef maður lýsir það drep að beinbrot hafa fylgt .
Tvær eru sakir ef tvennum er lýst .
Ef maður vill um frumhlaup sækja ,
og skal kveðja níu búa heiman .
Um drep það skal stefna og kveða á það að hinn hafi veitt honum beinbrot í því drepi og láta varða skóggang og telja rétt úr fé hins .
Ef maður heldur manni undir drep ,
og varðar það skóggang , og
skal kveðja níu vettvangsbúa heiman .
Ef tveir menn eða fleiri verða vegnir eða sárir eða drepnir svo að blátt eða rautt verður eftir , og úr sínum flokki hvor , þá eru ælir hvorirtveggju til dóms ef þeir bregðast frumhlaupum við , hvegi er um hlaup berst .
13. UM ÞAÐ HVERSU MAÐUR Á AÐ HEFNA SÁRA SINNA .
Sá maður er á er unnið á að hefna sín til þess Alþingis er hann er skyldur að sækja um sár sín , eða þann áverka sem hann fékk ,
og svo skulu þeir menn er vígs eigu að hefna .
Sá maður fellur óheilagur fyrir honum sjálfum , er á honum vann , og svo fyrir þeim mönnum öllum er honum fylgja til vettvangs ,
enda er rétt að aðrir menn hefni hans ef vilja , til jafnlengdar annars dægurs .
Þeir menn eigu vígs að hefna er aðiljar eru vígsakar og þeir menn er þeim fylgja til vettvangs ,
enda á hver að hefna er vill til annars dægurs .
Hvatki er maður gerir þess að öðrum manni er þar blæðir er á kemur ,
og skal sár lýsa , en ben ef að bana gerist .
Maður er óæll til dóms af vöðvasári ef hann er með votta inni staðinn .
En ef maður lýgst sári á eða særir hann sig sjálfur , svo og ef hann ræður annan mann til , og svo hvatki sem hann gerir þess er hann fær af því sár ,
og varðar það fjörbaugsgarð .
Sá á sök er hann kennir sár , eða sá hver er vill , þeirra manna er í andskotaflokki hans voru , þá er hann kvaðst sár fá ,
enda kvað hann á engan um .
Þess á maður og kost , ef hann verður fyrir drepi , að sækja um frumhlaup til fjörbaugsgarðs , ef honum þykja þau atkvæði betri að hann sæki eigi um drep .
Nú vænist hinn því að hann hafi drepið hann ,
og verður honum það ekki sakarvörn .
En það varðar fjörbaugsgarð ,
og skal það sækja sem annað illmæli .
Verður hann þar dæmdur skógarmaður af fjörbaugssökum tveim , ef í einn dóm eru báðar sóttar , því að einn er aðili að báðum sökunum .
14. UM VETTVANG .
Svo er mælt að það skal vettvangur vera er öru má skjóta á fjóra vega úr þeim stað er hið fyrsta hlaupið varð , hvort sem það er úti eða inni .
Og skal það vettvangur vera þó að þeir hafi víðara farið áður þeir eru skildir .
En þá eru þeir skildir , er aðrirtveggju eru lengra í brott komnir en ördrag úr þeim stað sem þeir hljópust síðast til , og er þeirra lengra á meðal en ördrag .
15. UM ÞAÐ HVERSU GRIÐA SKAL BEIÐA .
Hvarvetna þess er vegnar sakir standa óbættar á milli manna , enda vili menn sættast á þau mál , þeir er hlut eigu í hvorirtveggju sem fyrst má , sækjendur og verjendur og hollendur .
Hvatki er sátt þeirra dvelur , hverigra hluta er þeir beiða , þá eigu menn grið að selja hvorir öðrum ef menn vilja þess beitt hafa .
Það eru forn lög á Íslandi .
Ef vegandi beiðir sér griða nás nið eða nefa , eða hans frændur honum , eða sér , fyrir þriðju sól eftir vígið með votta og til heilla sátta við frændur veganda eða vini , fulltíða menn og frjálsborna , þá skulu þeir eigi griða varna ef að lögum er beitt .
Rétt er mönnum að beiða griða , hvargi er menn eru staddir þar er á unnin verk verða með mönnum , þótt engir sé vegnir .
En þá er að lögum beitt griða ef maður nefnir sér votta fimm eða fleiri , lögsjáendur , mann tólf vetra gamlan eða eldra , frjálsan og heimilisfastan , áttræðan eða yngra , svo heilan og hraustan að hann sé bæði vegandi og verjandi fjörs síns og fjár , skildi að valda eða skjóta af bendum boga .
" Nefni eg í það vætti að eg beiði N. og hans lagsmenn , vini og frændur , fégriða og fjörgriða mér og mínum mönnum til farnaðar og til ferðar , til happs og heilla sátta " .
Þá varðar fjörbaugsgarð ef hinn varnar griða , hvegi er hann svarar , eða svo ef hann svarar engu .
Þá skal votta að nefna ,
enda varðar jafnmikið hverngi veg sem hann varnar griða .
Griða er eigi varnað þótt sá nefni sér fimm votta eða fleiri .
Þá skal svo vanda að öllu sem hina fyrri tvo , þá er griða var beitt .
Sá nefnir í það vætti , er griða var beiddur , að hann spyr votta hins ,
og nefna hann eða vitnismenn , hvort þeir sé hyggjendur eða hollendur hvorratveggju þeirra er tveim megin standa að þessu máli , til heilla sátta og hollrar veislu , eða sé þeir eigi .
Nú svara þeir öngu vottarnir , eða öðru en þeim er í hug .
Þá varðar þeim fjörbaugsgarð ,
enda er hann eigi skyldur að láta þeim grið uppi .
Nú grunar hann það að þeir vili eigi heilar sáttir við hann .
Þá skal hann beiða með votta þann er hann beiddi griða , og svo votta hans , að þeir vinni tryggðaeið að því að eigi sé undirmál við hann af þeirra hendi , áður hann láti þeim grið uppi .
Nú vilja þeir vinna eiðinn ,
þá skal gefa eiðinn , nema þeir vili vinna hvorirtveggju .
Þá skulu þeir á þessa lund að kveða að vitni Guðs og allra heilagra manna hans :
" Sé mér Guð hollur ef eg satt segi , en gramur ef eg lýg .
Sé mér Guð hollur " .
Þá skal hann grið láta þeim uppi .
En ef þeir varna eiðanna , þá er það fjörbaugssök um eiðfallið .
En önnur fjörbaugssök er sú við þá er þeir vilja véla hann í nokkuru
og heitir það griðabrek ef eigi kemur fram .
En fram komið griðarof varðar skóggang .
Á sá sakir þær er griða beiddi , og við votta hans .
En ef hann vill eigi sækja , þá varðar honum þriggja marka útlegð .
Þá er sá sóknaraðili um griðarof er vígsökina á að sækja , eða um eiðfallið , nema sá sé allur einn er griðanna var beiddur og vígsakaraðilinn er eða áverkans .
En ef sá vill eigi sækja , þá varðar þeim fjörbaugsgarð er fyrst átti sökina um griðarof og um eiðfallið , er vottar eru þess er griða var beitt .
En ef þeir vilja eigi sækja , þá varðar skóggang þeim er fyrstur átti sökina um griðarof .
Sá á sök við hann er griðanna beiddi .
En honum varðar eigi við lög þótt hann stefni eigi um niðurfallið sakanna .
En því aðeins eyðir hann fyrir honum dæmd mál ef hann stefnir honum af sökum ,
enda væri sá eigi óæll til dóms .
Því aðeins eru mál hans mæt ef hann er eigi sannur að griðarofinu eða eiðfalli .
En ef sá er óæll er griða beiddi , enda sé hann sannur að griðarofi , þá á nokkur votta hans að stefna honum af sökum ,
og helpur honum þó því aðeins það , ef eigi er á vettvangi svo að hann verði æll til dóms ,
enda sé hann eigi að griðarofi sannur , hvort sem þeir eru á vettvangi eða annars staðar ,
enda sé griða að lögum beitt , hvergi sem beiðir .
Þá verður vegandi æll til dóms , nema nokkur þeirra sé er þrem mörkum væri gildur ef sekur yrði .
Þá varðar það eigi við lög að þeim sé griða varnað .
Griðabeiðandanum er rétt að stefna í þeim stað er griða var beitt um það er griða var varnað , og svo um það allt er þar gerist af . En ellegar nefna votta að og stefna að heimili þess er sóttur er ,
og skal þá við vottorð sækja , það er að var nefnt , ef eigi var stefnt , nema þar sé stefnt er griða var beitt ,
þá skal við hin fyrri vottorðin sækja jafnan , þau önnur sem fylgt hafa en stefnuvætti , nema á vettvangi sé ,
þá skulu vettvangsbúar níu skilja , þeir er um víg eða áverka eigu kviðu að bera um fjörbaugssakir og um skóggangssakir , en fimm annars staðar .
Nú ber svo að þeir vitu eigi hvort búar munu heiman kvaddir um víg eða áverka .
Þá skal spyrja með votta áður þeir skilast , hvort hinir ætli búa að kveðja heiman eða eigi , eða hverja þeir vilja kveðja .
Ef þeir svara engu eða óskilum , þá skal nefna að votta níu ,
og varðar lögvilla slíkt svo þar .
16. UM LÝSINGAR .
Lagalöstu þessa alla sem hér eru taldir , um víg og um sár og um drep og um öll frumhlaup , er rétt að lýsa fyrir vettvangsbúum fimm þeim er réttir sé , að leiðarlengd í níu búa kvið frá vettvangi og vanda að engu annars ,
og skal lýst fyrir þriðju sól þaðan í frá er frumhlaup eða áverk gerðust og þeir skildust .
Rétt er að lýsa sár og frumhlaup um nótt sem um dag , og svo á helgum tíðum og um langaföstu .
En ef ólýst verður eða ranglýst ,
og verða þeir þá ælir til dóms er áverk hafa veitt og eiga þingreitt .
Manni er rétt að lýsa frumhlaup á hönd öllum þeim mönnum er í andskotaflokki honum voru þá er hlaupið var til hans , ef hann veit eigi víst hver til hans hljóp .
Ef maður veit eigi víst hvort er hið meira eða hið minna sár ,
og skal þá hið meira sár lýsa ,
en slíkt varða bjargir hins sem sár reynist til .
Nú lýsir maður hið minna sár þar sem hið meira er ,
og skal þá svo sækja um björgina sem hið minna hafi verið sárið , en svo mest að öllu öðru sem áður .
Ef maður er einu sári sár ,
og skal hann einum á hönd lýsa .
Rétt er að hann lýsi tveim mönnum á hönd ef hann er tveim sárum sár ,
enda er rétt að hann lýsi þrimur á hendur ef hann er þrem sárum sár ,
en eigi skal hann fleirum á hendur lýsa en þrem mönnum , þótt hann sé fleirum sár sárunum .
Svo er enn mælt , hvort sem er að maður vill eigi lýsa eða má hann eigi lýsa ,
þá er honum rétt að hann seli öðrum manni í hönd að lýsa sár sitt , og svo þótt hann hafi fleiri sár en eitt , og allan þann áverka sem hann fékk .
Nú selur hann í hönd öðrum manni að lýsa áverka sinn , svo að hann er eigi í för sjálfur , enda lýsir sá rangt er til er fenginn ,
þá skal hann lýsa sjálfur í annað sinn .
En hann skal svo fara að lýsingu þeirri að hefja upp ferð sína á næsta hálfum mánaði er hann er fær og hafa lýst fyrir þriðju sól þaðan frá er hann kemur til vettvangs ,
enda skal hann slík atferli hafa öll hin sömu , ef hann fékk engan mann til fyrir öndverðu að lýsa , þá er hann varð vanhluti .
Nú selur hann svo í hönd öðrum manni að lýsa áverka sinn að hann er í ferð sjálfur ,
enda lýsi hinn rangt ,
þá á hann eigi að taka upp lýsingina í annað sinn .
17. UM ÞAÐ EF MAÐUR VERÐUR ÓMÁLI AF SÁRUM .
Ef maður er ómáli af áverkum eða óviti , þá skal sá maður lýsa er aðili væri vígsakar eftir hinn sára ef hann væri veginn .
Hann skal spyrja hinn sára mann hvort hann megi mæla eða eigi .
En síðan skal hann nefna votta að því að hinn má eigi mæla eða mælir eigi af viti ella .
Hann skal lýst hafa fyrir þriðju sól .
Nú verður sá eigi við staddur ,
og er rétt að lýsi hver er vill , þeirra er fulltingja hinum sára manni ,
og skal sá slík atferli hafa öll um lýsingina sem áður var tínt .
Nú batnar hinum sára manni ,
og er honum rétt að hafa hina sömu lýsing , enda er honum rétt að hann lýsi í annað sinn sjálfur , og fara svo að lýsingu sem hinum var til handa mælt er hann seldi lýsingina .
18. UM VÍG OG BENJAVÆTTI .
Ef maður verður veginn og er þar sakaraðili hjá , þá skal hann lýst hafa fyrir hina þriðju sól og svo nefnt votta að benjum .
Honum er rétt að lýsa jafnmörgum mönnum á hendur sem benjar eru á hinum dauða manni , þeim er í andskotaflokki hinum voru á þeim vettvangi er sá fékk banasár .
Svo skal nefna jafnmarga menn til benja , ef hann vill , sem benjar eru á hinum dauða .
Hann skal nefna votta að benjum og sýna þeim benjar og kveða á til hverrar benjar hann nefnir hvern þeirra sem hann vill til hafa nefndan .
" Nefni eg mér benjavætti þetta að lögum , " skal hann segja " eða þeim manni hverjum er njóta þarf þessa vættis . " ,
Honum er rétt að hafa eina vottnefnu þar sem hann nefnir votta að benjum ,
enda er rétt að nefna sér að hverri ben ef hann vill .
Benjavotta skal svo vanda að þeir sé réttir að tengdum í níu búa kvið við sakaraðilja .
Þeir eigu þá að bera hve margar benjar eru , en búakviður hverir sannir eru að .
Nú verða eigi vottar nefndir að benjum .
Þá skal búakviður skilja um hvorttveggja , hve margar benjar eru og hverir sannir eru að benjum ,
og skulu níu vettvangsbúar skilja það , heiman kvaddir , hverir sannir eru að benjum .
En fimm búar úr þeim sóknarkvið , þeir sem næstir búa vettvangi , skulu það skilja hve margar benjar eru .
19. UM BENJALÝSING .
Þar skal ben lýsa er þau sár finnast á dauðum manni er metast ætti sem hin meiri sár ef hann lifði . En eigi það sem hin smærri sár eru , nema því að einu ef kviður ber að það sár sé að bana orðið er minna sé ásýnar .
Nú verður aðili eigi við staddur þar er maður er veginn ,
og er rétt að hver lýsi er vill og nefni votta að benjum þeirra manna er fulltingja vilja aðiljanum , og hafa þau atferli sem áður var tínt .
Nú spyr sakaraðili vígið ,
og er honum rétt að hafa þann tilbúning sem aðrir menn hafa til búið .
Enda er honum rétt að hefja upp ferð sína á hinum fyrsta hálfum mánaði er hann spyr og hafa lýst og nefnda votta að benjum fyrir þriðju sól þaðan í frá er hann kemur til að lýsa .
Hann á þá kost að lýsa öðrum mönnum öllum á hendur en áður sé lýst , og svo nefna alla menn aðra til benja en áður væri nefndir .
Búakviður á þá að skilja hve margar benjar eru ,
enda er rétt að hann nýti það allt er áður hefir verið til búið , ef hann vill , hvort sem hann vill allt eða sumt .
Svo skal nefna votta að sárum sem að benjum ,
og svo skal þar vanda votta .
Ef rétt lýst er hinu meira sári á hönd manni , og gerist það að banasári ,
og þó að eigi verði víginu lýst á hönd hinum sama manni , þá er jöfn sókn til um bjargir sem áður .
Ef maður lýsir sár sín sjálfur og gerist það að vígi síðar ,
og á sakaraðili þó að lýsa víginu ef hann vill , og nefna votta að benjum .
Honum er þá rétt að lýsa öllum öðrum mönnum á hendur en áður sé lýst , og svo nefna aðra menn til benja ef hann vill .
20. UM ÁVERKALÝSING Á MANNFUNDUM .
Ef menn vinnast á á mannafundum þeim er búendur eru við staddir níu eða fleiri ,
og er rétt að lýsa fyrir þeim öllum saman níu sem þar eru við staddir , þó að þeir sé eigi vettvangsbúar , ef þeim eru eigi áverk kennd á þeim vettvangi .
Enda er rétt að lýsa fyrir fimm vettvangsbúum ef það þykir betra ,
og jafnrétt er þó að þeir hafi á vettvangi verið , ef þeim eru eigi áverk kennd .
Nú finnast menn á fjöllum eða á fjörðum ,
þá skal lýst fyrir þriðju sól þaðan frá er þeir koma af fjalli eða af firði .
Svo skal að lýsingu fara sem nú var tínt , hvar þess er lögsegjendur eða lögsjáendur ganga hvorntveggja veg frá vígi ,
en þeir eru lögsegjendur eða lögsjáendur karlar tólf vetra gamlir eða eldri , þeir er fyrir eiði og orði kunnu hyggja , frjálsir menn og heimilisfastir .
Hvar þess er menn ganga annan veg aðeins frá vígi , þá skal vegandi lýsa samdægris á hönd sér víginu nema það hafi verið á firði eða fjalli ,
þá skal hann samdægris lýst hafa er hann kemur þaðan .
Hann skal fara til næsta bæjar , þess er hann hyggi óhætt fjörvi sínu vera munu fyrir þá sök og aðra , og segja þar til lögföstum manni einum eða fleirum , og nefna hann á nafn hinn sára ef hann veit , og kveða á þessa lund að : " Fundur okkar N.N.-sonar varð , " og kveða á hvar varð ,
" og lýsi eg mér á hönd allan þann áverka sem á honum er unninn .
Lýsi eg sár ef að sárum gerist en víg ef að vígi gerist . "
Ef hann fer frá dauðum manni , þá skal hann hræ hylja svo að hvortki eti dýr né fuglar .
En ef hann hylur eigi hræ ,
og varðar það fjörbaugsgarð .
En ef hinn sári maður gengur síðan ördrag eða lengra frá fundi þeirra og segir öðrum mönnum frá þeirra skiptum , þá þarf hinn eigi að lýsa .
Nú má hinn sári maður eigi finna lögfasta menn svo að hann mæli við þá , þó að hann sé gengur ördrag eða lengra .
Þá skal sá er áverka hefir veitt honum lýsa fyrir búum fimm , þeim er næstir eru þeim stað sem hann er , þá er hann spyr dauða hins , ef hann lýsti eigi fyrr .
En ef hann lýsir annan veg en nú er tínt , og mest það þá sem morð að því að hinn hafði ekki þess til saka gert er hann félli fyrir því óheilagur .
Ef maður verður lostinn svo að eigi verður lýst ,
og er hann samvistum við þann mann er lostið hefir hann svo að hann væri fær í brott ,
og á hann eigi þá að taka rétt sinn úr fé hins , en það eru átta aurar hins fimmta tigar , nema sá maður sé er eigi á sjálfur forráð saka sinna , hvað sem til þess ber ,
og á sá þó að taka rétt sinn .
En þó á hinn að sækja sök sína til skógar ,
en rétt skal eigi dæma úr fé hins .
Ef manni verður svo banað , að eigi er á sýnt á honum , og finnast eigi ben á honum , verður hann eltur á vötn eða fyrir björg eða kyrktur eða kafður í nokkuru til heljar , og svo hverngi dauða er þeir deyða hann , þess er hann fær af þeirra völdum bana , enda verði honum eigi á sýnt , þá á því vígi eigi fleirum mönnum á hendur að lýsa en þrem , svo að þingfarar stöðvi , þó að fleiri hafi að verið .
21. UM ÞAÐ AÐ LÝSA FRUMHLAUP EÐA SÁR EÐA VÍG .
Sá maður er hann vill lýsa frumhlaup á hönd manni , eða drep eða sár eða víg , hann skal nefna sér votta tvo eða fleiri " í það vætti að eg lýsi lögmæt frumhlaup á hönd honum , "
og skal hann nefna þann er hljóp og svo þann er hlaupið var til og þann hinn þriðja er hann lýsir fyrir ,
og hann lýsir löglýsing .
Hann á kost að hafa hvort sem vill í einni lýsingu frumhlaup og þann áverka er í því hefir gerst ,
og skal hann á kveða hver áverk fylgt hafa , hvort sem verið hafa drep eða sár , eða hann særði hann því sári er að ben gerðist þar sem hann fékk bana af ,
enda skal hann þá eina sök úr gera ef hann hafði í einni lýsingu .
Honum er og rétt að lýsa sér hvort , frumhlaup og þann áverka sem fylgt hefir ,
og skulu þá vera tvær sakir .
Sá maður er hann lýsir sár eða drep , hann skal nefna sér votta tvo eða fleiri í það vætti að hann lýsir löglýsing á hönd N. , sár eða drep , og kveða á hver áverk eru , hvort sem er heilund eða holund eða mergund , eða svo þótt þau sé önnur áverk er metast ætti sem hin meiri sár , og svo ef hin minni sár eru , að nefna þá til .
Hann skal nefna hvorntveggja , þann er hann lýsir á hönd og þann er á er unnið , og þann hinn þriðja er hann lýsir fyrir .
Ef maður vill gera tvær sakir úr vígi og frumhlaupi , þá skal hann nefna sér votta tvo eða fleiri .
" Nefni eg í það vætti , " skal hann segja , " að eg lýsi lögmætt frumhlaup á hönd N. um það að hann hafi hlaupið til N. á þeim vettvangi er hann særði N. því sári er að ben gerðist , og N. fékk bana af .
Lýsi eg fyrir N. ,
lýsi eg löglýsing . "
Síðan skal hann enn nefna votta tvo eða fleiri " í það vætti að eg lýsi á hönd N. að hann hafi veitt sár N. það er að ben gerðist og að bana á þeim vettvangi er N. hljóp lögmætu frumhlaupi til N.
Lýsi eg löglýsing . "
Rétt er að maður lýsi frumhlaup og áverk fyrir öllum búum saman , þeim er hann náir fundi í einum stað og hann vill fyrir hafa lýst ,
og skal hann nefna þá alla á nafn og hafa þar eina vottnefnu ,
enda er rétt að lýsa fyrir sérhverjum .
Þar er maður vill selja öðrum í hönd að lýsa frumhlaup eða áverk , þá skulu þeir nefna votta sín á milli að því að hann selur hinum í hönd að lýsa frumhlaup eða áverk löglýsingu .
Selur hann að lögum ,
en hinn tekur að lögum .
Nýmæli :
Ef áverk eru ólýst , þá skal eina sökina úr gera og kveðja búa heiman um það hvort hann hafi hlaupið lögmætu frumhlaupi til hans og særðan hann því sári sem hann hafði fengið ,
og skal á kveða hvert sár er .
Nýmæli :
Þar er menn skulu lýsa frumhlaup eða áverk , þá er rétt að lýsa þaðan frá er sá atburður gerðist , þann hinn sama dag og þá nótt er þá kemur eftir og tvo daga þaðan frá og tvær nætur .
Þá er lýst fyrir hina þriðju sól sem að kveður í uppsögu ef það er rétt skilt sem þar kveður að .
22. UM KVAÐIR .
Það er mælt um sakir þær allar sem hér eru taldar , um frumhlaup og um sár og um víg og lagalöstu alla er á þeim vettvangi gerast er á unnin verk verða með mönnum bæði í ráðum og tilför og aðvist og fyrirstöðu ,
þar skal um það allt kveðja til níu vettvangsbúa heiman .
En ef maður drepur mann ,
og er það stefnusök ,
og skal kveðja til tylftarkviðar goða þann er sá er í þingi með er sóttur er , nema allt verði á einum vettvangi , drep og sár eða víg .
Þá skulu vettvangsbúar níu , heiman kvaddir , skilja um hvorttveggja ,
en tylftarkviðar á jafnan á þingi að kveðja þar sem hann kemur til saka .
Ef maður hefnir drepi dreps á hinum sama vettvangi , svo að þeir bregðast frumhlaupum við , þá skal tylftarkviður skilja um drepin .
En vettvangsbúar níu , heiman kvaddir , skulu um það skilja hvor þeirra fyrri hljóp .
Nú hefnir maður drepi dreps á öðrum vettvangi .
Þá skulu enn tylftarkviðir skilja um drep ,
en sá skal kveðja er síðar drap fimm heimilisbúa sína á þingi til bjargkviðar sér að bera um það hvor þeirra fyrri var drepinn .
Nú hefnir maður vígi eða sárum dreps á öðrum vettvangi .