package br.unicamp.iel.tycho.tagger.vlmm.util;

/**
 * Tagger exception.
 *
 * @author Luiz Veronesi
 * @version 1.0 Jul 31, 2013
 */
public class TaggerException extends Exception {

	private static final long serialVersionUID = 9113437764169073215L;

	public TaggerException() {
	}
	
	public TaggerException(Exception e) {
		super((Throwable) e);
	}
	
	public TaggerException(String msg) {
		super(msg);
	}
}
