package br.unicamp.iel.tycho.tagger.vlmm.pos;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * Represents a sentence for the POS file.
 *
 * @author Luiz Veronesi
 * @version 1.0 30/01/2013
 */
public class PosSentence {

	private String id;
	
	private List<PosToken> tokens;
	
	public PosSentence() {};
	
	public PosSentence(List<PosToken> tokens) {
	    setTokens(tokens);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<PosToken> getTokens() {
		return tokens;
	}

	public void setTokens(List<PosToken> tokens) {
		this.tokens = tokens;
	}
	
	public void addToken(PosToken posToken) {
		if (this.tokens == null) {
			this.tokens = new ArrayList<>();
		}
		
		this.tokens.add(posToken);
	}
	
	/**
	 * Get sentence as a String with token values separated by a white space.
	 * 
	 * @return
	 */
	public String toString() {
		
		StringBuffer sb = new StringBuffer();
		
		for (PosToken token : tokens) {
			sb.append(token.getName());
			
			if (!StringUtils.isEmpty(token.getTag())) {
				sb.append("/");
				sb.append(token.getTag());
			}
			
			sb.append(" ");
		}
		
		return sb.toString().trim();
	}
}
