package br.unicamp.iel.tycho.tagger.vlmm.exec;

import java.util.Date;

import br.unicamp.iel.tycho.tagger.vlmm.util.TextReader;

public class Runner {

	public static void main(String[] args) throws Exception {
		
		String str = "Ofereco a Vossa Majestade as Reflexoes sobre a vaidade dos homens ...";
   	    System.out.println("words:" + TextReader.getWordsFromUntaggedText(str));

		String inputDirPos = "c:/work/projects/view/docs/texts/pos/txt/";
		
		TextTagger t = new TextTagger(inputDirPos, Boolean.TRUE);
		
		Date dt = new Date();
		
		System.out.println(t.tag(str));
		
		System.out.println(new Date().getTime() - dt.getTime());
	}
	
}
