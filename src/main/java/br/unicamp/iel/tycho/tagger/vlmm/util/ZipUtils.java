package br.unicamp.iel.tycho.tagger.vlmm.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Utilities methods to manage compressed zip files for the VLMM tagger exporter/importer tool. 
 *
 * @author Luiz Veronesi
 * @version 1.0 01/08/2013
 */
public class ZipUtils {

	/**
	 * Create the compressed file inserting the specified files located at a specific location.
	 * 
	 * @param outputFile
	 * @param filesLocation
	 * @param files
	 * @throws IOException
	 */
	public static void compress(String outputFile, String filesLocation, String... files) throws IOException {
		
		OutputStream output = new FileOutputStream(new File(outputFile));
		ZipOutputStream zipOutput = new ZipOutputStream(output);
		
		for (String file : files) {
			addToZipFile(filesLocation, file, zipOutput);
		}
		
		zipOutput.close();
		output.close();
	}
	
	/**
	 * Add a file to a ZipOutputStream.
	 * 
	 * @param dirLocation
	 * @param fileName
	 * @param zos
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private static void addToZipFile(String dirLocation, String fileName, ZipOutputStream zos) throws FileNotFoundException, IOException {

		File file = new File(dirLocation + fileName);
		FileInputStream fis = new FileInputStream(file);
		ZipEntry zipEntry = new ZipEntry(fileName);
		zos.putNextEntry(zipEntry);

		byte[] bytes = new byte[1024];
		int length;
		while ((length = fis.read(bytes)) >= 0) {
			zos.write(bytes, 0, length);
		}

		zos.closeEntry();
		fis.close();
	}
}
