package br.unicamp.iel.tycho.tagger.vlmm.exec;


/**
 * Argumentos para rodar a classe main.
 * 
 * Alterei o nome de todas as variaveis retirando _ e criei os getters e setters.
 * 
 * @author Luiz Veronesi
 * @version 1.0 05/09/2012
 * 
 * 
 */
public class MainArgs {

    private boolean trainFile;
    private String trainFileName;
    
    private boolean testFile;
    private String testFileName;
    
    private boolean testFileNotTagged;
    
    private boolean saveTrainedChainFile;
    private String saveTrainedChainFileName;
    
    private boolean loadTrainedChainFile;
    private String loadTrainedChainFileName;
    
    private boolean saveCountedChainFile;
    private String saveCountedChainFileName;
    
    private boolean loadCountedChainFile;
    private String loadCountedChainFileName;
    
    private boolean saveSplittedCorpusFile;
    private String saveSplittedCorpusFileName;
    
    private boolean selectPartTrainCorpus;
    private double partTrainCorpus;
    private int size;
    private boolean corpusFile;
    private String corpusFileName;
    private double divideCorpuInTrainAndTest; // The number specified being the size of the training corpus (interval [0-1]).
    private String delimiter;
    
    // removed to Functions.FINAL_PUNCTUATION_SYMBOLS
    //private Set<String> finalPunctuationSymbols; // Set manually in 'init()'. TODO: put it on a config file.    

	public boolean isTrainFile() {
		return trainFile;
	}

	public void setTrainFile(boolean trainFile) {
		this.trainFile = trainFile;
	}

	public String getTrainFileName() {
		return trainFileName;
	}

	public void setTrainFileName(String trainFileName) {
		this.trainFileName = trainFileName;
	}

	public boolean isTestFile() {
		return testFile;
	}

	public void setTestFile(boolean testFile) {
		this.testFile = testFile;
	}

	public String getTestFileName() {
		return testFileName;
	}

	public void setTestFileName(String testFileName) {
		this.testFileName = testFileName;
	}

	public boolean isTestFileNotTagged() {
		return testFileNotTagged;
	}

	public void setTestFileNotTagged(boolean testFileNotTagged) {
		this.testFileNotTagged = testFileNotTagged;
	}

	public boolean isSaveTrainedChainFile() {
		return saveTrainedChainFile;
	}

	public void setSaveTrainedChainFile(boolean saveTrainedChainFile) {
		this.saveTrainedChainFile = saveTrainedChainFile;
	}

	public String getSaveTrainedChainFileName() {
		return saveTrainedChainFileName;
	}

	public void setSaveTrainedChainFileName(String saveTrainedChainFileName) {
		this.saveTrainedChainFileName = saveTrainedChainFileName;
	}

	public boolean isLoadTrainedChainFile() {
		return loadTrainedChainFile;
	}

	public void setLoadTrainedChainFile(boolean loadTrainedChainFile) {
		this.loadTrainedChainFile = loadTrainedChainFile;
	}

	public String getLoadTrainedChainFileName() {
		return loadTrainedChainFileName;
	}

	public void setLoadTrainedChainFileName(String loadTrainedChainFileName) {
		this.loadTrainedChainFileName = loadTrainedChainFileName;
	}

	public boolean isSaveCountedChainFile() {
		return saveCountedChainFile;
	}

	public void setSaveCountedChainFile(boolean saveCountedChainFile) {
		this.saveCountedChainFile = saveCountedChainFile;
	}

	public String getSaveCountedChainFileName() {
		return saveCountedChainFileName;
	}

	public void setSaveCountedChainFileName(String saveCountedChainFileName) {
		this.saveCountedChainFileName = saveCountedChainFileName;
	}

	public boolean isLoadCountedChainFile() {
		return loadCountedChainFile;
	}

	public void setLoadCountedChainFile(boolean loadCountedChainFile) {
		this.loadCountedChainFile = loadCountedChainFile;
	}

	public String getLoadCountedChainFileName() {
		return loadCountedChainFileName;
	}

	public void setLoadCountedChainFileName(String loadCountedChainFileName) {
		this.loadCountedChainFileName = loadCountedChainFileName;
	}

	public boolean isSaveSplittedCorpusFile() {
		return saveSplittedCorpusFile;
	}

	public void setSaveSplittedCorpusFile(boolean saveSplittedCorpusFile) {
		this.saveSplittedCorpusFile = saveSplittedCorpusFile;
	}

	public String getSaveSplittedCorpusFileName() {
		return saveSplittedCorpusFileName;
	}

	public void setSaveSplittedCorpusFileName(String saveSplittedCorpusFileName) {
		this.saveSplittedCorpusFileName = saveSplittedCorpusFileName;
	}

	public boolean isSelectPartTrainCorpus() {
		return selectPartTrainCorpus;
	}

	public void setSelectPartTrainCorpus(boolean selectPartTrainCorpus) {
		this.selectPartTrainCorpus = selectPartTrainCorpus;
	}

	public double getPartTrainCorpus() {
		return partTrainCorpus;
	}

	public void setPartTrainCorpus(double partTrainCorpus) {
		this.partTrainCorpus = partTrainCorpus;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public boolean isCorpusFile() {
		return corpusFile;
	}

	public void setCorpusFile(boolean corpusFile) {
		this.corpusFile = corpusFile;
	}

	public String getCorpusFileName() {
		return corpusFileName;
	}

	public void setCorpusFileName(String corpusFileName) {
		this.corpusFileName = corpusFileName;
	}

	public double getDivideCorpuInTrainAndTest() {
		return divideCorpuInTrainAndTest;
	}

	public void setDivideCorpuInTrainAndTest(double divideCorpuInTrainAndTest) {
		this.divideCorpuInTrainAndTest = divideCorpuInTrainAndTest;
	}

	public String getDelimiter() {
		return delimiter;
	}

	public void setDelimiter(String delimiter) {
		this.delimiter = delimiter;
	}
}
