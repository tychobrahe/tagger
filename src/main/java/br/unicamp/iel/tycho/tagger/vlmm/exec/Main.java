package br.unicamp.iel.tycho.tagger.vlmm.exec;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;

import br.unicamp.iel.tycho.tagger.vlmm.loader.ModelLoader;
import br.unicamp.iel.tycho.tagger.vlmm.loader.ModelTrainer;
import br.unicamp.iel.tycho.tagger.vlmm.model.Model;
import br.unicamp.iel.tycho.tagger.vlmm.pos.PosDocument;
import br.unicamp.iel.tycho.tagger.vlmm.pos.PosReader;
import br.unicamp.iel.tycho.tagger.vlmm.pos.PosSentence;
import br.unicamp.iel.tycho.tagger.vlmm.pos.PosToken;
import br.unicamp.iel.tycho.tagger.vlmm.util.Functions;
import br.unicamp.iel.tycho.tagger.vlmm.util.TextReader;
import br.unicamp.iel.tycho.tagger.vlmm.viterbi.Viterbi;

/**
 * @author Fabio Kepler
 * @version 1.0 2005
 * 
 * @author Luiz Veronesi
 * @version 2.0 Set/2012
 * 
 * @author Fabio Kepler
 * @version 3.0 Dec/2012
 * 
 * @author Luiz Veronesi
 * @version 4.0
 * 
 * Defined ways of using tagger via command line:
 * 
 * 1- tag given text training the model, optionally writing the model
 * 2- tag given text loading the model, optionally writing the model
 * 3- train the model and writing it
 * 
 * Arguments:
 * T or train: train the model
 * L or load: load the model
 * w or write: write the model (optional and writes a trained_model.data file
 * t or tag: tag the text
 * 
 * first argument: trained modelfile name or folder containing files to train the model
 * second argument: filename containing text to be tagged or single sentence to be tagged
 * 
 * Examples:
 * tagger.jar -T -w /data/www/pos/
 * tagger.jar -T -w -t /data/www/pos/file.pos /tmp/file_containing_sentences_to_tag.txt
 * tagger.jar -T -w -t /data/www/pos/file.pos "I want to tag this sentence"
 * tagger.jar -L -w /data/www/pos/trained_model.data
 * tagger.jar -L -w -t /data/www/pos/trained_model.data "I want to tag this sentence"
 * tagger.jar -L -w -t /data/www/pos/trained_model.data /tmp/file_containing_sentences_to_tag.txt
 */
public class Main {

    private static final String[] VALID_ARGUMENTS = new String[] { "T",
            "train", "L", "load", "w", "write", "t", "tag" };

    private static final String SENTENCE = "sentence";

    private static final String FILE = "file_$";

    public static void main(String[] args) throws Exception {

        Map<String, Object> arguments = checkArguments(args);

        Model model = null;

        // load from training file
        if (arguments.containsKey("L") || arguments.containsKey("load")) {
            model = ModelLoader.execute((String) arguments.get("file_0"));
        }

        // train using tagged corpus (a file or a directory with many files)
        if (arguments.containsKey("T") || arguments.containsKey("train")) {
            System.out.println((String) arguments.get("file_0"));
            //trainCorpus = new Corpus();
            model = ModelTrainer.execute((String) arguments.get("file_0"), Boolean.FALSE);
            //model = ModelTrainer.load((String) arguments.get("file_0"));
        }

        if (model == null)
            throw new Exception("Problem loading model. Wrong filename: "
                    + arguments.get("file_0"));
        
        if (arguments.containsKey("t") || arguments.containsKey("tag")) {
        	Viterbi viterbi = new Viterbi(model);
        	String inputFileOrDir = (String) arguments.get("file_1");
        	System.out.println("Tagging file " + inputFileOrDir);
        	
    		try {
				PosReader posReader = new PosReader(false);
				List<PosSentence> goldSentences = new ArrayList<>();
				if (posReader.isDirectory(inputFileOrDir)) {
    				List<PosDocument> documents = posReader.read(inputFileOrDir);
    				for (PosDocument document : documents) {
    				    goldSentences.addAll(document.getSentences());
    				}
				} else {
				    goldSentences = posReader.readFile(inputFileOrDir).getSentences();
				}
				
				List<PosSentence> testSentences = new ArrayList<>();
				for (PosSentence goldSentence : goldSentences) {
				    testSentences.add(new PosSentence(goldSentence.getTokens()));
				}
				
		        int n = 0;
		        for (PosSentence s : goldSentences) {
		            n += s.getTokens().size();
		        }
		        //System.err.println("Number of words (pre-tagging): " + n);

				viterbi.tag(testSentences);
				
				// TODO: save file
				// TODO: check accuracy
				Functions.calculateAccuracy(goldSentences, testSentences, model);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.err.println("ERRO");

				TextReader textReader = new TextReader();
				List<List<String>> sentences = textReader.read(inputFileOrDir);
				
				List<List<String>> tags = viterbi.getTags(sentences);
				// TODO: save file
			}
        }


        // List<List<String>> tags = viterbi.tag(testCorpus);
        // ou
        // viterbi.tag(testCorpus); // modifica testCorpus, acrescentando tags
        // aos
        // tokens
        // ou
        // List<List<String>> tags = viterbi.tag(tokens);
        // ou
        // List<List<Token>> tags = viterbi.tag(tokens);
    }

    /**
     * A valid argument is one of the Strings inside VALID_ARGUMENTS variable
     * containing first symbol as "-" (minus) or a file or a directory or a
     * String whose length is greater than the greatest argument one.
     * 
     * First element must be a valid argument from the array. Number of
     * arguments must be equal or greater than 2.
     * 
     * @param args
     * @return Map<String, Object>
     */
    private static Map<String, Object> checkArguments(String[] args)
            throws Exception {

        if (args.length < 2) {
            throw new Exception("Wrong number of arguments. Must be two.");
        }

        if (!ArrayUtils.contains(VALID_ARGUMENTS, args[1].replace("-", ""))) {
            throw new Exception("Invalid argument: " + args[1].replace("-", ""));
        }

        for (String arg : args) {
            if (arg.contains("-")) {
                if (!ArrayUtils.contains(VALID_ARGUMENTS, arg.replace("-", ""))) {
                    throw new Exception("Invalid argument: " + arg);
                }
            } else {
                File file = new File(arg);
                if (!file.exists() && arg.length() < 5)
                    throw new Exception("Invalid file or directory: " + arg);
            }
        }

        Map<String, Object> arguments = getArguments(args);

        // TODO: improve argument validation to prevent erroneous calls

        return arguments;
    }

    /**
     * Create a map containing the arguments.
     * 
     * @param args
     * @return Map<String, Object>
     */
    private static Map<String, Object> getArguments(String[] args) {

        Map<String, Object> map = new HashMap<>();

        Integer i = 0;
        for (String arg : args) {
            if (arg.contains("-")) {
                map.put(arg.replace("-", ""), Boolean.TRUE);
            } else {
                File file = new File(arg);
                if (!file.exists()) {
                    map.put(SENTENCE, arg);
                } else {
                    map.put(FILE.replace("$", i.toString()), arg);
                    i++;
                }
            }
        }

        return map;
    }
}
