package br.unicamp.iel.tycho.tagger.vlmm.exec;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;

import br.unicamp.iel.tycho.tagger.vlmm.loader.ModelExporter;
import br.unicamp.iel.tycho.tagger.vlmm.loader.ModelTrainer;
import br.unicamp.iel.tycho.tagger.vlmm.model.Model;
import br.unicamp.iel.tycho.tagger.vlmm.pos.PosDocument;
import br.unicamp.iel.tycho.tagger.vlmm.pos.PosReader;
import br.unicamp.iel.tycho.tagger.vlmm.pos.PosSentence;
import br.unicamp.iel.tycho.tagger.vlmm.pos.PosToken;

/**
 * This class reads POS tagged files using the Icelandic Treebank format and converts
 * them to POS sentences to run the tagger. 
 * 
 * @author Luiz Veronesi
 * @version 1.0 Aug, 2013
 */
public class IcelandicReader {

	private static final String SPLIT_DELIMITER = "\t"; 
	
	public static void main(String[] args) throws Exception {
	
		String inputDir = "data/icepahc-v0.9/tagged/";
		String outputFile = "c:/temp/icelandic.zip";
		
		IcelandicReader reader = new IcelandicReader();
		List<PosDocument> documents = reader.read(inputDir);

		List<PosSentence> sentences = new ArrayList<>();
		for (PosDocument posDocument : documents) {
			for (PosSentence sentence : posDocument.getSentences()) {
				if (CollectionUtils.isEmpty(sentence.getTokens())) continue;
				sentences.add(sentence);
			}
		}

		Model model = ModelTrainer.execute(sentences);
		
		ModelExporter exporter = new ModelExporter();
		exporter.execute(model, outputFile);
		
		TextTagger tagger = new TextTagger(model);
		List<PosToken> tokens = tagger.tag("Í flestum löndum setja menn á bækur annað tveggja þann fróðleik er þar innan lands hefir gjörst – eða þann annan er minnisamlegastur þykir þó að annars staðar hafi heldur gjörst –");
		
		for (PosToken posToken : tokens) {
			System.out.println(posToken.getName() + "\t" + posToken.getTag());
		}
	}
	
	/**
	 * Processes all text files inside a directory converting them to a list of PosDocument objects.
	 * 
	 * @param dirName
	 * @return List<PreDocument>
	 * @throws Exception
	 */
	public List<PosDocument> read(String name) throws Exception {
		
		List<PosDocument> documents = new ArrayList<>();
		
		File dir = new File(name);
		
		if (!dir.isDirectory()) {
			documents.add(read(new File(name)));
			return documents;
		}
		
		File[] files = dir.listFiles();
		
		for (File file : files) {
			if (!file.isFile()) continue;
			
			try {
				documents.add(read(file));
			} catch (Exception e) {
				System.out.println(file.getName() + "\n\n");
				e.printStackTrace();
			}
		}
		
		return documents;
	}
	
	/**
	 * Read a POS text file and convert to a PosDocument.
	 * 
	 * @param file
	 * @return PosDocument
	 * @throws Exception
	 */
	private PosDocument read(File file) throws Exception {
		
		PosDocument document = new PosDocument();
		document.setName(file.getName());
		
		List<String> lines = FileUtils.readLines(file, "UTF-8");
		
		PosSentence sentence = new PosSentence();
		
		for (String line : lines) {
			if (line.trim().isEmpty()) continue;

			PosToken posToken = this.getToken(line);
			if (posToken.getTag().equals(PosReader.FINAL_PUNCT)) {
				sentence.addToken(posToken);
				document.addSentence(sentence);
				
				sentence = new PosSentence();
			} else {
				sentence.addToken(posToken);
			}
		}
		
		document.addSentence(sentence);
		
		return document;
	}
	
	/**
	 * Each line has a token.
	 * 
	 * @param line
	 * @return List<PosToken>
	 */
	private PosToken getToken(String line) {
		StringTokenizer st = new StringTokenizer(line, SPLIT_DELIMITER);
		
		String value = st.nextToken();
		String tag = st.nextToken();
		
		if (Pattern.matches("\\p{Punct}", tag)) {
			tag = "PONFP";
		}
		
		return new PosToken(value, tag);
	}
}
