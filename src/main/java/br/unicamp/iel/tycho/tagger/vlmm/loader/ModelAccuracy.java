package br.unicamp.iel.tycho.tagger.vlmm.loader;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import br.unicamp.iel.tycho.tagger.vlmm.exec.TextTagger;
import br.unicamp.iel.tycho.tagger.vlmm.pos.PosReader;
import br.unicamp.iel.tycho.tagger.vlmm.pos.PosSentence;
import br.unicamp.iel.tycho.tagger.vlmm.pos.PosToken;

/**
 * Tests the model accuracy training it with a specified file and tagging its own sentences
 * against themselves.
 *
 * @author Luiz Veronesi
 * @version 1.0 31/07/2013
 */
public class ModelAccuracy {

	public static void main(String[] args) throws Exception {
		
		//String inputDirPos = "c:/work/projects/view/docs/texts/pos/txt/";
		String inputDirPos = "data/tb.pos";

		ModelAccuracy accuracy = new ModelAccuracy();
		accuracy.test(inputDirPos);
	}
	
	private void test(String inputDirPos) throws Exception {
		
		PosReader posReader = new PosReader(Boolean.FALSE);
		List<PosSentence> sentences = posReader.getSentences(inputDirPos);

		TextTagger tagger = new TextTagger(sentences);
		
		int total = sentences.size();
		int match = 0;
		for (PosSentence sentence : sentences) {
			
			if (sentence.getTokens().isEmpty()) {
				total--;
				continue;
			}
			
			List<String> tags = tagger.tag(sentence);
			
			List<String> innerTags = new ArrayList<>();
			for (PosToken token : sentence.getTokens()) {
				innerTags.add(token.getTag());
			}
			
			if (CollectionUtils.isEqualCollection(tags, innerTags)) {
				match++;
			}
		}
		
		System.out.println("total: " + total);
		System.out.println("match: " + match);
	}
}
