package br.unicamp.iel.tycho.tagger.vlmm.util;

import java.util.Map;

import br.unicamp.iel.tycho.tagger.vlmm.model.Node;

/**
 * Wrapper used to import a node.
 * It contains reference to a node, its key and its branches references.
 *
 * @author Luiz Veronesi
 * @version 1.0 26/06/2013
 */
public class NodeImportWrapper {

	private String key;
	
	private Node node;
	
	private Map<String, String> branches;

	public NodeImportWrapper() {
	}

	public NodeImportWrapper(String key, Node node, Map<String, String> branches) {
		this.key = key;
		this.node = node;
		this.branches = branches;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Node getNode() {
		return node;
	}

	public void setNode(Node node) {
		this.node = node;
	}

	public Map<String, String> getBranches() {
		return branches;
	}

	public void setBranches(Map<String, String> branches) {
		this.branches = branches;
	}

}
