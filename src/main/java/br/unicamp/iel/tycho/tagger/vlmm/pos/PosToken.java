package br.unicamp.iel.tycho.tagger.vlmm.pos;

/**
 * Represents a token inside a POS file.
 *
 * @author Luiz Veronesi
 * @version 1.0 30/01/2013
 */
public class PosToken {

	private String id;
	
	private String name;
	
	private String tag;
	
	private Double probability;

	public PosToken() {
    }

	public PosToken(String name, String tag) {
		this.name = name;
		this.tag = tag;
	}

	public PosToken(String name, String tag, Double probability) {
		this.name = name;
		this.tag = tag;
		this.probability = probability;
	}

	public PosToken(String id, String name, String tag) {
		this.id = id;
		this.name = name;
		this.tag = tag;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String type) {
		this.tag = type;
	}
	
	public Double getProbability() {
		return probability;
	}

	public void setProbability(Double wordTagProb) {
		this.probability = wordTagProb;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if (!(obj instanceof PosToken)) return false;
		
		PosToken p2 = (PosToken) obj;
		
		if (this.getName().equals(p2.getName()) && this.getTag().equals(p2.getTag())) return true;
		
		return false;
	}
	
	public String toString() {
		
		if (this.getProbability() != null) {
			return this.getName() + "|" + this.getTag() + "|" + this.getProbability();
		}
		
		return this.getName() + "|" + this.getTag();
	}
}
