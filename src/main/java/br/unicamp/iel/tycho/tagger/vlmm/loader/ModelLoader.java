package br.unicamp.iel.tycho.tagger.vlmm.loader;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;

import br.unicamp.iel.tycho.tagger.vlmm.model.AffixTree;
import br.unicamp.iel.tycho.tagger.vlmm.model.ContextTree;
import br.unicamp.iel.tycho.tagger.vlmm.model.MatrixAccumulator;
import br.unicamp.iel.tycho.tagger.vlmm.model.Model;
import br.unicamp.iel.tycho.tagger.vlmm.pos.PosToken;
import br.unicamp.iel.tycho.tagger.vlmm.util.ModelOptions;

/**
 * TODO: improve this file when decided how the output format will be. 
 *
 * @author Luiz Veronesi
 * @version 1.0 11/07/2013
 */
public class ModelLoader extends ModelFactory {

	public static Model execute(String filename) throws Exception {
		return execute(new File(filename));
	}
	
	/**
	 * Loads the model from the trained file.
	 * 
	 * @return
	 * @throws Exception
	 */
	public static Model execute(List<PosToken> posTokens, AffixTree affixTree, ContextTree contextTree) throws Exception {
		
        ModelLoader loader = new ModelLoader();
        
        Model model = new Model(new ModelOptions());
        model.setLeftContextTree(contextTree);
        model.setWordSuffixTree(affixTree);

        for (PosToken token : posTokens) {
            model.getWordToTagCounter().increment(token.getName(), token.getTag());
            model.getTagToWordCounter().increment(token.getTag(), token.getName());
        }
        
        loader.compute(model);
        
        return model;
	}
	
	/**
	 * Loads the model from the trained file.
	 * 
	 * @return
	 * @throws Exception
	 * 
	 * FIXME: review this, it is necessary to send correct file names, obviously
	 */
	public static Model execute(File file) throws Exception {
		
		ModelLoader loader = new ModelLoader();
		
		Model model = new Model(new ModelOptions());
		model.setLeftContextTree(loader.loadContextTree(null));
		model.setWordSuffixTree(loader.loadAffixTree(null));
		model.setWordToTagCounter(loader.loadWords(null));
		model.setTagToWordCounter(loader.loadTags(null));

		loader.compute(model);
		
		return model;
	}
	
	/**
	 * Load words data from trained data file.
	 * 
	 * @return StateToStateMatrix
	 * @throws Exception
	 */
	public MatrixAccumulator loadWords(String filename) throws Exception {

		List<String> lines = FileUtils.readLines(new File(filename), "UTF-8");

		MatrixAccumulator wordStates = new MatrixAccumulator();
		for (String line : lines) {
			
			String[] values = line.split(" ");
			
			String fromState = values[0];
			Double probability = new Double(values[1]);
			String toState = values[2];
			
			wordStates.add(fromState, toState, probability);
		}
		
		return wordStates;
	}
	
	/**
	 * Load tags data from trained data file.
	 * 
	 * @return StateToStateMatrix
	 * @throws Exception
	 */
	public MatrixAccumulator loadTags(String filename) throws Exception {
		
		List<String> lines = FileUtils.readLines(new File(filename), "UTF-8");
		
		MatrixAccumulator tagWordProbabilities = new MatrixAccumulator();
		for (String line : lines) {
			
			String[] values = line.split(" ");
			
			String fromState = values[0];
			Double probability = new Double(values[1]);
			String toState = values[2];
			
			if (!probability.equals(0.0)  && !toState.isEmpty() && !fromState.isEmpty())
			tagWordProbabilities.add(fromState, toState, probability);
		}
		
		return tagWordProbabilities;
	}
	
    /**
     * Loads the affix tree.
     *  
     * @return AffixTree
     */
	public AffixTree loadAffixTree(String filename) throws Exception {
    	AffixTreeModelLoader loader = new AffixTreeModelLoader();
    	return loader.execute(filename);
    }
    
    /**
     * Loads the affix tree.
     *  
     * @return AffixTree
     */
	public ContextTree loadContextTree(String filename) throws Exception {
    	ContextTreeModelLoader loader = new ContextTreeModelLoader();
    	return loader.execute(filename);
    }
}
