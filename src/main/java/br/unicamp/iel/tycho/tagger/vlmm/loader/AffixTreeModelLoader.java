package br.unicamp.iel.tycho.tagger.vlmm.loader;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import br.unicamp.iel.tycho.tagger.vlmm.model.AffixTree;
import br.unicamp.iel.tycho.tagger.vlmm.model.Node;
import br.unicamp.iel.tycho.tagger.vlmm.util.NodeImportWrapper;

/**
 * Loads the affix tree from a file.
 * Data is put at each line delimited by @@ in the following format:
 * 
 * 1.4@@1@@1@@s~2.1^@@P+D-F-P,1;@@
 * depth.levelOrder @@ probability @@ number of branches @@ branches delimited by ^ @@ sprouts delimited by ;
 *
 * @author Luiz Veronesi
 * @version 1.0 26/06/2013
 */
public class AffixTreeModelLoader {

    /**
     * Loads the affix tree from a file.
     * Data is put at each line delimited by @@ in the following format:
     * 
     * 1.4@@1@@1@@s~2.1^@@P+D-F-P,1;@@
     * depth.levelOrder @@ probability @@ number of branches @@ branches delimited by ^ @@ sprouts delimited by ;
     * 
     * @return AffixTree
     */
    public AffixTree execute(String filename) throws Exception {

    	List<String> lines = FileUtils.readLines(new File(filename), "UTF-8");

        // First read the file and convert to a list of temporary class, also check for invalid line format.
    	List<AffixTreeLine> affixLines = this.readAffixTreeLines(lines);
    	
    	// Key is a composition of depth and order, the first element from each node at the file. Example: 2.1 -> 2 is depth, 1 is order
    	Map<String, NodeImportWrapper> mapNodes = this.convertAffixLines(affixLines);
        
    	Map<String, String> mapNodeNames = new HashMap<>();
    	
    	Node root = null;
    	for (Map.Entry<String, NodeImportWrapper> entry : mapNodes.entrySet()) {

    		NodeImportWrapper nodeWrapper = entry.getValue();
    		Node node = nodeWrapper.getNode();
    		
    		if (mapNodeNames.containsKey(entry.getKey())) {
    			node.setName(mapNodeNames.get(entry.getKey()));
    		} else {
    			node.setName(entry.getKey());
    		}
    		
    		for (Map.Entry<String, String> branch : nodeWrapper.getBranches().entrySet()) {
    			node.addBranch(branch.getKey(), mapNodes.get(branch.getValue()).getNode());
    			
    			mapNodeNames.put(branch.getValue(), branch.getKey());
    		}
    		
    		// ROOT
    		if (entry.getKey().equals("0")) {
    			root = node;
    		}
    	}
    	
    	return new AffixTree(root);
    }

    /**
     * Convert a list of affix tree lines to a map wrapping the nodes.
     * 
     * @param affixLines
     * @return Map<String, NodeImportWrapper>
     */
    private Map<String, NodeImportWrapper> convertAffixLines(List<AffixTreeLine> affixLines) throws Exception {

    	// Ordering by the key it is easier to make the tree
        Map<String, NodeImportWrapper> nodes = new TreeMap<>();

        for (int i = 0; i < affixLines.size(); i++) {
        	
        	AffixTreeLine affixLine = (AffixTreeLine) affixLines.get(i);
        	
        	Integer depth = 0;
        	
        	// split depth from level order, but level order is not importante at this moment
        	String[] depthSplit = affixLine.getDepthOrder().split(AffixTreeLine.DEPTH_DELIM);
        	if (depthSplit.length == 2) {
        		depth = new Integer(depthSplit[0]);
        	}

        	Node node = new Node(affixLine.getProbability(), depth, false);
			
        	Map<String, String> mapBranches = new HashMap<>();
	    	StringTokenizer branches = new StringTokenizer(affixLine.getBranches(), AffixTreeLine.BRANCHES_DELIM);
	    	while (branches.hasMoreTokens()) {
	    	    
	    		/*
	    		 * Splits this branch into 2: first element is the first character of the node at branch, second element 
	    		 * is the combination of depth and level order for this node.
	    		 */
	    		String[] branchData = branches.nextToken().split(AffixTreeLine.BRANCH_DATA_DELIM);

	    		if (branchData.length != 2) {
	    			throw new Exception("ERROR[5]: affix tree file on line " + i + ": invalid line format");
	    	    }

	    		mapBranches.put(branchData[0], branchData[1]);
	    	}

	    	// Fifth field [sprouts]
	    	if (!StringUtils.isEmpty(affixLine.getSprouts())) {

	    		//P,0.0576923076923077;P+D-F,0.711538461538462;P+D-F-P,0.211538461538462;P+D-P,0.0192307692307692;
		    	StringTokenizer sprouts = new StringTokenizer(affixLine.getSprouts(), AffixTreeLine.SPROUT_DELIM);
		    	
		    	while (sprouts.hasMoreTokens()) {
		    	
		    		String sprout = sprouts.nextToken();

		    		// P,0.0576923076923077
		    		String[] innerSproutData = sprout.split(AffixTreeLine.SPROUT_DATA_DELIM);
		    		if (innerSproutData.length != 2) {
		    			throw new Exception("ERROR[6]: affix tree file on line " + i + ": invalid line format");
		    	    }
		    	    
		    		node.addSprout(innerSproutData[0], new Double(innerSproutData[1]));
		    	}
	    	}
	    	
	    	nodes.put(affixLine.getDepthOrder(), new NodeImportWrapper(affixLine.getDepthOrder(), node, mapBranches));
        }
        
        return nodes;
    }
    
    /**
     * Read all lines inside the file and transform them to affix tree lines according to the inner class
     * defined below.
     * 
     * @param lines
     * @return List<AffixTreeLine>
     */
    private List<AffixTreeLine> readAffixTreeLines(List<String> lines) throws Exception {
    	
        List<AffixTreeLine> affixLines = new ArrayList<>();
        for (int i = 0; i < lines.size(); i++) {
        	
        	String[] lineTokens = lines.get(i).split(AffixTreeLine.LINE_DELIM, -1);
        	
            if (lineTokens.length != 6) {
	    		throw new Exception("ERROR[1]: affix tree file on line " + i + ": invalid line format.");
	    	}

            AffixTreeLine affixLine = new AffixTreeLine();
            
            affixLine.setDepthOrder(lineTokens[0]);
	    	if (!affixLine.getDepthOrder().equals("0") && affixLine.getDepthOrder().split(AffixTreeLine.DEPTH_DELIM).length != 2) {
	    		throw new Exception("ERROR[2]: affix tree file on line " + i + ": invalid line format.");
	    	}
            
            affixLine.setProbability(new Double(lineTokens[1]));
            affixLine.setNumBranches(new Integer(lineTokens[2]));

            affixLine.setBranches(lineTokens[3]);
            
            if (!StringUtils.isEmpty(affixLine.getBranches())) {
		    	if (!affixLine.getNumBranches().equals(affixLine.getBranches().split(AffixTreeLine.BRANCHES_DELIM).length)) {
		    		throw new Exception("ERROR[4]: affix tree file on line " + i + ": invalid line format.");
		    	}
            }
            
            affixLine.setSprouts(lineTokens[4]);
            
            affixLines.add(affixLine);
        }
        
        return affixLines;
    }
    
    /**
     * Class used for reading AffixTree file lines and make conversion easier.
     *
     * @author Luiz Veronesi
     * @version 1.0 24/06/2013
     */
    private class AffixTreeLine {
    	
        public static final String LINE_DELIM = "@@";
        public static final String DEPTH_DELIM = "\\.";
        public static final String BRANCHES_DELIM = "\\^";
        public static final String BRANCH_DATA_DELIM = "~";
        public static final String SPROUT_DELIM = ";";
        public static final String SPROUT_DATA_DELIM = ",";
    	
        private String depthOrder;
        
        private Double probability;
        
        private Integer numBranches;
        
        private String branches;
        
        private String sprouts;

		public String getDepthOrder() {
			return depthOrder;
		}

		public void setDepthOrder(String depth) {
			this.depthOrder = depth;
		}

		public Double getProbability() {
			return probability;
		}

		public void setProbability(Double probability) {
			this.probability = probability;
		}

		public Integer getNumBranches() {
			return numBranches;
		}

		public void setNumBranches(Integer numBranches) {
			this.numBranches = numBranches;
		}

		public String getBranches() {
			return branches;
		}

		public void setBranches(String branches) {
			this.branches = branches;
		}

		public String getSprouts() {
			return sprouts;
		}

		public void setSprouts(String sprouts) {
			this.sprouts = sprouts;
		}        
    }
}
