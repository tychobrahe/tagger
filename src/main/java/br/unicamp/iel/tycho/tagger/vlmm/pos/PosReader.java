package br.unicamp.iel.tycho.tagger.vlmm.pos;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.io.FileUtils;

/**
 * Reader for the POS files.
 *
 * @author Luiz Veronesi
 * @version 1.0 30/01/2013
 */
public class PosReader {

	private static final String SPLIT_DELIMITER = "@"; 
	
	public static final String FINAL_PUNCT = "PONFP";
	
	private Boolean tychoPattern = Boolean.FALSE;
	
	public PosReader() {
	}

	public PosReader(Boolean tychoPattern) {
		this.tychoPattern = tychoPattern;
	}
	
	/**
	 * Returns true if filename is a directory.
	 * 
	 * @param filename
	 * @return
	 */
	public boolean isDirectory(String filename) {
	    return (new File(filename)).isDirectory();
	}

	/**
	 * Processes all text files inside a directory converting them to a list of PosDocument objects.
	 * 
	 * @param dirName
	 * @return List<PreDocument>
	 * @throws Exception
	 */
	public List<PosDocument> read(String name) throws Exception {
		
		List<PosDocument> documents = new ArrayList<>();
		
		File dir = new File(name);
		
		if (!dir.isDirectory()) {
			documents.add(read(dir));
			return documents;
		}
		
		File[] files = dir.listFiles();
		
		for (File file : files) {
			if (!file.isFile()) continue;
			
			try {
				documents.add(read(file));
			} catch (Exception e) {
				System.out.println(file.getName() + "\n\n");
				e.printStackTrace();
			}
		}
		
		return documents;
	}
	
	/**
	 * Process a single file.
	 * 
	 * @param fileName
	 * @return PreDocument
	 * @throws Exception
	 */
	public PosDocument readFile(String fileName) throws Exception {
		return read(new File(fileName));
	}
	
	/**
	 * Read a POS text file and convert to a PosDocument.
	 * 
	 * @param file
	 * @return PosDocument
	 * @throws Exception
	 */
	private PosDocument read(File file) throws Exception {
		
		PosDocument document = new PosDocument();
		document.setName(file.getName());
		
		List<String> lines = FileUtils.readLines(file, "UTF-8");
		
		for (String line : lines) {
			
			PosSentence sentence = new PosSentence();
			
			if (line.trim().isEmpty() || (tychoPattern && !line.contains(FINAL_PUNCT))) continue;
			
			List<PosToken> tokens = readTokens(line);
			
			if (tychoPattern) {
				// remove last element from list, it contains the PONFP token
			    // FIXME: warning! Assumption is that there is one sentence per line!
				tokens.remove(tokens.size() - 1);

				// pop last element, it contains sentence identification
				PosToken idToken = tokens.remove(tokens.size() - 1);
				
				sentence.setId(idToken.getName());
			}
			
			// FIXME: should not skip these tags.
//			if (tokens.size() == 2 
//					&& tokens.get(0).getTag().equals("NUM") && tokens.get(1).getTag().equals(".")) {
//				continue;
//			}
			
			sentence.setTokens(tokens);

			document.addSentence(sentence);
		}
		
		return document;
	}
	
	/**
	 * Get POS tokens from a line.
	 * 
	 * @param line
	 * @return List<PosToken>
	 */
	private List<PosToken> readTokens(String line) {
		
		List<PosToken> tokens = new ArrayList<PosToken>();
		
		StringTokenizer st = new StringTokenizer(line);
		
		while (st.hasMoreTokens()) {
			
			String val = st.nextToken();
			
			String[] tokenSplit = val.split("/");
			
			if (tokenSplit.length < 2) continue;
			
			if (!tokenSplit[1].equals("CODE")) {
				tokens.add(new PosToken(tokenSplit[0], tokenSplit[1]));
			}
		}

		List<PosToken> adjustedTokens = new ArrayList<PosToken>();
		
		// Join splitted tokens
		for (int i = 0; i < tokens.size(); i++) {
			PosToken token = tokens.get(i);
			
			if (token.getName().endsWith(SPLIT_DELIMITER)) {
				
				PosToken nextToken = tokens.get(i + 1);
				
				if (!nextToken.getName().startsWith(SPLIT_DELIMITER)) continue;
				
				String splittedValue = token.getName().replace(SPLIT_DELIMITER, "") + nextToken.getName().replace(SPLIT_DELIMITER, "");
				
				PosToken splittedToken = new PosToken(splittedValue, token.getTag() + "+" + nextToken.getTag());
				adjustedTokens.add(splittedToken);
				
				i++;
				
			} else {
				adjustedTokens.add(tokens.get(i));
			}
		}
		
		return adjustedTokens;
	}
	
	/**
	 * Get all sentences from read POS document.
	 * 
	 * @param input
	 * @return
	 * @throws Exception
	 */
	public List<PosSentence> getSentences(String input) throws Exception {

		List<PosDocument> documents = this.read(input);

		List<PosSentence> sentences = new ArrayList<>();
		for (PosDocument document : documents) {
			sentences.addAll(document.getSentences());
		}
		
		return sentences;
	}
}
