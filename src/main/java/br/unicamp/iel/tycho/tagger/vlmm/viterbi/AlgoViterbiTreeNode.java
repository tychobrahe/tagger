package br.unicamp.iel.tycho.tagger.vlmm.viterbi;

import java.util.AbstractMap;

import javax.swing.tree.DefaultMutableTreeNode;

/**
 * Tree node for AlgoViterbi Tree.
 * It contains as object a Map entry containing the word and its associated tag as values.
 * 
 * @author Luiz Veronesi
 * @version 1.0 23/10/2012
 */
public class AlgoViterbiTreeNode extends DefaultMutableTreeNode implements Cloneable {

	private static final long serialVersionUID = 5512248583132450380L;
	
	private String word;
	private String tag;
	
	public AlgoViterbiTreeNode(String word, String tag) {
		super(new AbstractMap.SimpleEntry<String, String>(word, tag));
		this.word = word;
		this.tag = tag;
	}
	
	public String getWord() {
		return this.word;
	}
	
	public String getTag() {
		return this.tag;
	}
	
	public AlgoViterbiTreeNode clone() {
		return (AlgoViterbiTreeNode) super.clone();
	}
}
