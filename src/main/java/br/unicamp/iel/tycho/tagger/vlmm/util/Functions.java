package br.unicamp.iel.tycho.tagger.vlmm.util;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.util.FastMath;

import br.unicamp.iel.tycho.tagger.vlmm.model.Accumulator;
import br.unicamp.iel.tycho.tagger.vlmm.model.Model;
import br.unicamp.iel.tycho.tagger.vlmm.pos.PosSentence;
import br.unicamp.iel.tycho.tagger.vlmm.pos.PosToken;

/**
 * This class contains general functions for the VLMM Tagger.
 * 
 * @author Luiz Veronesi
 * @version 2.0 Added deprecation to several methods and static constants. Added
 *          method for
 */
public class Functions {

    @Deprecated
    public static final String ENDL = "\n";

    @Deprecated
    public static final String[] PUNCTUATION_SYMBOLS = { ".", ",", "(" };

    @Deprecated
    public static final String[] FINAL_PUNCTUATION_SYMBOLS = { ".", "?", "!", ";", ":", "..." };

    /**
     * Calculates the probability of n1 to n2 using log.
     * 
     * @param n1
     * @param n2
     * @return Double
     */
    public static Double calculateProbByLog(Number n1, Number n2) {
        return FastMath.log(n1.doubleValue()) - FastMath.log(n2.doubleValue());
    }

    /**
     * Get the Kullback-Leibler divergence of two log probabilities. This is a
     * pseudo-KL Divergence, since it does not include the sum over 't'.
     * 
     * @param n1
     * @param n2
     * @return Double
     */
    public static Double getKLDivergence(Number n1, Number n2) {
        return FastMath.exp(n1.doubleValue()) * (n1.doubleValue() - n2.doubleValue());
    }

    public static double calculateAccuracy(List<PosSentence> goldSentences, List<PosSentence> testSentences, Model model) throws IOException {

        int num_correct = 0;
        int num_wrong = 0;
        int num_unknown_words_correct = 0;
        int num_unknown_words_wrong = 0;
        int num_unknown_words = 0;
        int num_ambiguous_words_correct = 0;
        int num_ambiguous_words_wrong = 0;

        Accumulator mostWrongAmbiguousWords = new Accumulator();

        String fileName = "errors.info";
        File errorsFile = new File(fileName);

        if (!errorsFile.canWrite()) {
            System.err.println("Warning: could not open file '" + fileName + "' for writing tagging errors.");
        }

        StringBuffer sb = new StringBuffer();
        sb.append("CONTEXT WORD/WRONG_TAG%CORRECT_TAG CONTEXT" + ENDL);
        
        for (int i = 0; i < testSentences.size(); i++) {
            List<PosToken> goldTokens = goldSentences.get(i).getTokens();
            List<PosToken> testTokens = testSentences.get(i).getTokens();
            for (int t = 0; t < testTokens.size(); t++) {
                String word = testTokens.get(t).getName();
                String goldTag = goldTokens.get(t).getTag();
                String testTag = testTokens.get(t).getTag();

                if (testTag.equals(goldTag)) {
                    num_correct++;

                    if (!model.isWordKnown(word)) {
                        num_unknown_words_correct++;
                        num_unknown_words++;
                    }

                    if (model.isWordAmbiguous(word))
                        num_ambiguous_words_correct++;

                } else {
                    num_wrong++;
                    
                    String know_word = "word(!ambiguous)";
                    if (!model.isWordKnown(word)) {
                        num_unknown_words_wrong++;
                        num_unknown_words++;
                        know_word = "word(U)";
                    }
                    if (model.isWordAmbiguous(word)) {
                        num_ambiguous_words_wrong++;
                        know_word = "word";
                        mostWrongAmbiguousWords.increment(word);
                    }

                    sb.append("On sentence " + (i + 1) + " " + know_word + " " + (t + 1) + ": ");
                    sb.append(((t > 1) ? (testTokens.get(t - 2).getName() + "/" + testTokens.get(t - 2).getTag()) : "")
                            + " ");
                    sb.append(((t > 0) ? (testTokens.get(t - 1).getName() + "/" + testTokens.get(t - 1).getTag()) : "")
                            + " ");
                    sb.append(word + "/" + testTag + "%" + goldTag + " ");
                    sb.append(((t + 1 < testTokens.size()) ? (testTokens.get(t + 1).getName() + "/" + testTokens.get(
                            t + 1).getTag()) : "")
                            + ENDL);
                }
            }
        }

        FileUtils.writeStringToFile(errorsFile, sb.toString());
//        FileUtils.write(errorsFile, sb.toString());

        System.out.println("[Stats of tagging]");
        System.out.println("\tMost wrong tagged words:");
        int top_max = 10;
        int top = 0;

        for (Map.Entry<String, Double> entry : MapUtils.entriesSortedByValueDescending(mostWrongAmbiguousWords)) {
            if (top > top_max)
                break;
            top++;

            System.out.println("\t\t" + entry.getValue() + ":" + entry.getKey());
        }

        int num_words = num_correct + num_wrong;
        int num_known_words = num_words - num_unknown_words;
        int num_known_words_correct = num_correct - num_unknown_words_correct;
        int num_known_words_wrong = num_wrong - num_unknown_words_wrong;
        double percent_known_words = (double) num_known_words / (double) num_words;
        double known_words_accuracy = (double) ((double) num_known_words_correct / (double) num_known_words);

        // printf("Known words: %d (%g%%) Correct tagged: %d Wrong tagged: %d Accuracy: %g%%\n",
        // num_known_words, 100*percent_known_words, num_known_words_correct,
        // num_known_words_wrong, known_words_accuracy*100);

        sb = new StringBuffer();
        sb.append("Known words: " + num_known_words + " (" + 100 * percent_known_words + "%)");
        sb.append(" Correct tagged: " + num_known_words_correct);
        sb.append(" Wrong tagged: " + num_known_words_wrong);
        sb.append(" Accuracy: " + known_words_accuracy * 100 + "%");
        System.out.println(sb.toString());

        if (num_unknown_words != 0) {
            double percent_unknown_words = (double) num_unknown_words / (double) num_words;
            double unknown_words_accuracy = (double) ((double) num_unknown_words_correct / (double) num_unknown_words);

            // printf("Unknown words: %d (%g%%) Correct tagged: %d Wrong tagged: %d Accuracy: %g%%\n",
            // num_unknown_words, 100*percent_unknown_words,
            // num_unknown_words_correct, num_unknown_words_wrong,
            // unknown_words_accuracy*100);

            sb = new StringBuffer();
            sb.append("Unknown words: " + num_unknown_words + " (" + 100 * percent_unknown_words + "%)");
            sb.append(" Correct tagged: " + num_unknown_words_correct);
            sb.append(" Wrong tagged: " + num_unknown_words_wrong);
            sb.append(" Accuracy: " + unknown_words_accuracy * 100 + "%");
            System.out.println(sb.toString());
        }

        double num_ambiguous_words = (double) (num_ambiguous_words_correct + num_ambiguous_words_wrong);
        double percent_ambiguous_words = num_ambiguous_words / (double) num_words;
        double ambiguous_words_accuracy = (double) ((double) num_ambiguous_words_correct / num_ambiguous_words);

        // printf("Ambiguous words: %g (%g%%) Correct tagged: %d Wrong tagged: %d Accuracy: %g%%\n",
        // num_ambiguous_words, 100*percent_ambiguous_words,
        // num_ambiguous_words_correct, num_ambiguous_words_wrong,
        // ambiguous_words_accuracy*100);

        sb = new StringBuffer();
        sb.append("Ambiguous words: " + num_ambiguous_words + " (" + 100 * percent_ambiguous_words + "%)");
        sb.append(" Correct tagged: " + num_ambiguous_words_correct);
        sb.append(" Wrong tagged: " + num_ambiguous_words_wrong);
        sb.append(" Accuracy: " + ambiguous_words_accuracy * 100 + "%");
        System.out.println(sb.toString());

        System.out.println("Total tags: " + (num_correct + num_wrong) + " Total correct tags: " + num_correct + " Total wrong tags: " + num_wrong);
        double accuracy = (double) num_correct / (double) num_words;
        System.out.println("Total accuracy: " + (accuracy * 100) + "%");

        return accuracy;
    }

}
