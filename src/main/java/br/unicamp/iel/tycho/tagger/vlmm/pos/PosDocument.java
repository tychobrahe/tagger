package br.unicamp.iel.tycho.tagger.vlmm.pos;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a document for POS files.
 *
 * @author Luiz Veronesi
 * @version 1.0 30/01/2013
 */
public class PosDocument {

	private String name;
	
	private List<PosSentence> sentences;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<PosSentence> getSentences() {
		return sentences;
	}

	public void setSentences(List<PosSentence> sentences) {
		this.sentences = sentences;
	}
	
	public void addSentence(PosSentence sentence) {
		if (sentences == null) {
			sentences = new ArrayList<PosSentence>();
		}
		
		sentences.add(sentence);
	}
}
