package br.unicamp.iel.tycho.tagger.vlmm.model;

import java.io.Serializable;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;

import com.google.gson.annotations.Expose;

import br.unicamp.iel.tycho.tagger.vlmm.util.Functions;

/**
 * Affix tree for the VLMM Tagger.
 * 
 * @author Fabio Kepler
 * @version 1.0 unknown
 */
public class AffixTree implements Serializable {

    private static final long serialVersionUID = -1119016296998950295L;

    @Expose
    private Node root;
    
    @Expose
    private String name;
    
    @Expose
    private Integer density;
    
    @Expose
    private Double cutValue;

    public AffixTree() {
        this.density = 1;
        this.cutValue = 0.0;
        init();
    }

    public AffixTree(int density) {
        this.density = density;
        this.cutValue = 0.0;
        init();
    }

    public AffixTree(Node root) {
        this.root = root;
    }

    public void init() {
        root = new Node(0.0, 0, false);
    }

    /**
     * Adds a branch pointing to a sprout to the tree.
     * 
     * @param branch
     *            contains a reversed sequence of tags occurring in the training
     *            corpus.
     * @param tag
     *            is the sprout.
     */
    public void addBranch(String tag, String branch) {
        if (root.getSprouts().containsKey(tag)) {
            root.getSprouts().get(tag).incrementCount();
        } else {
            root.getSprouts().put(tag, new Sprout(1.0));
        }
        root.incrementCount();

        if (branch.isEmpty()) {
            return;
        }

        Node parent = root;
        for (Character branchItem : branch.toCharArray()) {
            Map<String, Node> branches = parent.getBranches();
            if (!branches.containsKey(String.valueOf(branchItem))) {
                branches.put(String.valueOf(branchItem), new Node());
            }
            Node newNode = branches.get(String.valueOf(branchItem));
            newNode.setName(String.valueOf(branchItem));
            newNode.incrementCount();
            newNode.setDepth(parent.getDepth() + 1);
            newNode.setParent(parent);
            // Add sprout and increment counter.
            Map<String, Sprout> newSprouts = newNode.getSprouts();
            if (!newSprouts.containsKey(tag)) {
                newSprouts.put(tag, new Sprout());
            }
            newSprouts.get(tag).incrementCount();
            // newNode.setSprouts(newSprouts);
            // parent.setBranches(branches);
            parent = newNode;
        }
        // if (branch.length() > 9) {
        // System.out.println("Long branch: " + branch + "; length: " +
        // branch.length() + "; sprout: " + destSprout);
        // System.out.println(getSproutsOfBranch(branch));
        // System.out.println("root branches: " + root.getBranches());
        // }
    }

    public Double getSproutCount(String sproutName, String branch) {
        if (branch.isEmpty()) {
            Sprout sprout = root.getSprouts().get(sproutName);
            if (sprout == null) {
                return 0.0;
            }
            return sprout.getCount();
        }

        Double lastCount = 0.0;
        Node currentNode = root;
        for (int i = 0; i < branch.length(); i++) {
            if (currentNode.getBranches().isEmpty())
                break;
            char c = branch.charAt(i);
            if (!currentNode.getBranches().containsKey(String.valueOf(c)))
                break;
            currentNode = currentNode.getBranches().get(String.valueOf(c));
            if (currentNode.getSprouts().containsKey(String.valueOf(sproutName))) {
                lastCount = currentNode.getSprouts().get(String.valueOf(sproutName)).getCount();
            }
        }

        return lastCount;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the density
     */
    public Integer getDensity() {
        return density;
    }

    /**
     * @return the cutValue
     */
    public Double getCutValue() {
        return cutValue;
    }

    /**
     * The density is the minimum frequency a node must have to stay in the tree
     * when the method {@shake} is called.
     * 
     * @param density
     *            the density to set
     */
    public void setDensity(Integer density) {
        this.density = density;
    }

    /**
     * The @cutValue is used when pruning the tree (method {@prune}).
     * 
     * @param cutValue
     *            the cutValue to set
     */
    public void setCutValue(Double cutValue) {
        this.cutValue = cutValue;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Remove nodes occurring less times than the specified density.
     */
    public void shake() {
        shake(false);
    }

    /**
     * Remove nodes and sprouts occurring less times than the specified density.
     */
    public void shake(boolean apply_to_sprouts) {
        Node currentNode;
        Deque<Node> nodeQueue = new ArrayDeque<Node>();
        // Going Breadth First down the tree.
        // Cut off rare nodes.
        nodeQueue.addLast(this.root);
        while (!nodeQueue.isEmpty()) {
            currentNode = nodeQueue.pop();
            if (currentNode.getCount() < getDensity()) {
                if (currentNode.getParent() == null) { // so currentNode is the
                                                       // root
                    currentNode.getBranches().clear();
                    return;
                }
                currentNode.getParent().getBranches().remove(currentNode.getName());
            } else {
                if (apply_to_sprouts) {
                    for (Iterator<Entry<String, Sprout>> iterator = currentNode.getSprouts().entrySet().iterator(); iterator
                            .hasNext();) {
                        Entry<String, Sprout> entry = iterator.next();
                        if (entry.getValue().getCount() < getDensity()) {
                            currentNode.setCount(currentNode.getCount() - entry.getValue().getCount());
                            iterator.remove();
                        }
                    }
                }
                if (currentNode.getSprouts().isEmpty() && currentNode.getParent() != null) {
                    currentNode.getParent().getBranches().remove(currentNode.getName());
                } else {
                    nodeQueue.addAll(currentNode.getBranches().values());
                }
            }
        }
    }

    /**
     * Remove nodes if the KL Divergence with respect to their parents is less
     * than the cut value (#getCutValue()).
     * 
     * This implementation traverses the tree in a top-down manner. Gives a
     * slightly different result than the bottom-up approach.
     */
    public boolean prune() {
        System.out.println("[Prunning " + getName() + "Context Tree]");

        Stack<Node> nodesStack = new Stack<Node>();
        nodesStack.push(root);
        Node currentNode;

//        int times_pruned = 0, times_not_pruned = 0;

        while (!nodesStack.isEmpty()) {
            currentNode = nodesStack.pop();

            for (Iterator<Entry<String, Node>> iteratorNode = currentNode.getBranches().entrySet().iterator(); iteratorNode
                    .hasNext();) {

                Entry<String, Node> entryNode = iteratorNode.next();
                Node subnode = entryNode.getValue();
                Double delta = 0.0;

                for (Iterator<Entry<String, Sprout>> iteratorSprout = subnode.getSprouts().entrySet().iterator(); iteratorSprout
                        .hasNext();) {

                    Entry<String, Sprout> entrySprout = iteratorSprout.next();
                    String sproutName = entrySprout.getKey();
                    Sprout sprout = entrySprout.getValue();

                    double parentSproutLogprob = Functions.calculateProbByLog(currentNode.getSproutCount(sproutName),
                            currentNode.getCount());

                    double childSproutLogprob = Functions.calculateProbByLog(sprout.getCount(), subnode.getCount());

                    double divergence = Functions.getKLDivergence(childSproutLogprob, parentSproutLogprob);

                    divergence *= subnode.getCount(); // Weighted divergence
                    delta += divergence;
                }

                // Decide pruning or not.
                if (delta < getCutValue()) { // prune
                    iteratorNode.remove(); // Remove subnode
//                    times_pruned++;
                } else {
                    // Leave this node on the tree.
                    nodesStack.push(subnode);
//                    times_not_pruned++;
                }
            }

            // FIXME: Was the negation of the if. Can't remember why, but now it
            // seems wrong. Changed it.
            if (currentNode.getBranches().isEmpty()) {
                currentNode.setFinalLeaf(true);
            }
        }
//        System.out.println("\tTimes pruned: " + times_pruned + " Times not pruned: " + times_not_pruned);
        return true;
    }

    public Set<String> getSproutsOfBranch(String branch) {
        Node node = root;

        for (int i = 0; i < branch.length(); i++) {
            if (!node.getBranches().containsKey(String.valueOf(branch.charAt(i))))
                break;
            node = node.getBranches().get(String.valueOf(branch.charAt(i)));
        }

        return node.getSprouts().keySet();
    }

    public void printStatistics() {
        int max_size = 0;
        int sum_size = 0;
        int num_branches = 0;
        Node deepestNode = null;
        LinkedList<Node> deepestNodes = new LinkedList<>();
        Accumulator branches_sizes = new Accumulator(); // key: branch size;
                                                        // data: number of
                                                        // branches with this
                                                        // size.

        LinkedList<Entry<String, Node>> queue = new LinkedList<>(this.root.getBranches().entrySet());
        while (!queue.isEmpty()) {
            Entry<String, Node> parent = queue.poll();
            // String parentName = parent.getKey();
            Node parentNode = parent.getValue();

            if (parentNode.isFinalLeaf() || parentNode.getBranches().isEmpty()) {
                sum_size += parentNode.getDepth();
                branches_sizes.increment(String.valueOf(parentNode.getDepth()));
                num_branches++;
                if (parentNode.getDepth() > max_size) {
                    max_size = parentNode.getDepth();
                    deepestNode = parentNode;
                    deepestNodes.clear();
                }
                if (parentNode.getDepth() == max_size) {
                    deepestNodes.add(parentNode);
                }
            } else {
                queue.addAll(parentNode.getBranches().entrySet());
            }
        }

        Double average_size = (double) sum_size / (double) num_branches;
        System.out.println("[" + getName() + "Tree Statistics: ]");
        Double k = getCutValue();
        System.out.println("\tCut value (K): " + k + "; density:  " + this.density);
        System.out.println("\tNumber of branches: " + num_branches);
        System.out.println("\tAverage branch size: " + average_size);
        System.out.print("\tGreatest branch size: " + max_size + " (e.g.: ");
        LinkedList<String> path = new LinkedList<>();
        Node node = deepestNode;
        while (node != null && node.getName() != null) {
            path.push(node.getName());
            node = node.getParent();
        }
        System.out.println(path);

        for (Entry<String, Double> e : branches_sizes.entrySet()) {
            System.out.println("\tNumber of branches with size (" + e.getKey() + "): " + e.getValue());
        }
        // voutLow + "\tBranches with this size:" + endl;
        // if (vout.output_priority <= vout.allowed_priority) {
        // for (unsigned int i = 0; i < max_branches.size(); i++) {
        // voutLow + "\t\t" + ToString(max_branches[i]) + endl;
        // }
        // }
        System.out.println("[End Tree Statistics]");
    }

}
