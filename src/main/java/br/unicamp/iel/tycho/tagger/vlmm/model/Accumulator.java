package br.unicamp.iel.tycho.tagger.vlmm.model;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.math3.util.FastMath;

/**
 * Contador de palavras.
 * 
 * @author Luiz Veronesi
 * @version 1.0 29/08/2012
 * 
 * @author Fabio N. Kepler
 * @version 3.0 24/06/2013
 */
public class Accumulator extends LinkedHashMap<String, Double> {

    private static final long serialVersionUID = 5506275151034511887L;
    private Double total = 0.0;

    public Accumulator() {
        super();
    }

    public Accumulator(String key) {
        this.set(key, 0.0);
    }

    public Accumulator(String key, Double value) {
        this.set(key, value);
    }

    public Double set(String key, Double value) {
        this.total += value - this.get(key);
        return this.put(key, value);
    }

    public Double add(String key, Double value) {
        this.total += value;
        if (this.containsKey(key)) {
            value = this.get(key) + value;
        }
        return this.put(key, value);
    }

    public Double increment(String key) {
        this.total += 1.0;
        Double value = 1.0;
        if (this.containsKey(key)) {
            value = this.get(key) + value;
        }
        return this.put(key, value);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.util.HashMap#get(java.lang.Object)
     */
    @Override
    public Double get(Object key) {
        Double count = super.get(key);
        if (count == null) {
            return 0.0;
        } else {
            return count;
        }
    }

    public Double getLog(Object key) {
        return FastMath.log(this.get(key)); // Zero is handled
    }
    
    public Double getTotal() {
        return total;
    }
    
    public Double getTotalLog() {
        return FastMath.log(this.getTotal());
    }

    public Double discount(String key) {
        return add(key, -1.0);
        /*
    	Double value = 0.0;
        if (this.containsKey(key)) {
        	value = this.get(key);
        	this.total -= value;
        }
        return value;
        */
    }
    
    @Override
	public Double remove(Object key) {
    	Double value = super.remove(key);
    	this.total -= value;
    	return value;
	}

}
