package br.unicamp.iel.tycho.tagger.vlmm.exec;

import java.util.ArrayList;
import java.util.List;

import br.unicamp.iel.tycho.tagger.vlmm.loader.ModelTrainer;
import br.unicamp.iel.tycho.tagger.vlmm.model.Model;
import br.unicamp.iel.tycho.tagger.vlmm.pos.PosSentence;
import br.unicamp.iel.tycho.tagger.vlmm.pos.PosToken;
import br.unicamp.iel.tycho.tagger.vlmm.util.TaggerException;
import br.unicamp.iel.tycho.tagger.vlmm.util.TextReader;
import br.unicamp.iel.tycho.tagger.vlmm.viterbi.Viterbi;

/**
 * Recupera as tags relacionadas a um texto utilizando o algoritmo de Viterbi.
 * 
 * @author Luiz Veronesi
 * @version 1.0 15/09/2012
 */
public class TextTagger {

	private Model model;
	
	private Viterbi vit;
	
	public TextTagger(String inputDir, Boolean tychoPattern) throws Exception {
		model = ModelTrainer.execute(inputDir, tychoPattern);
   	    vit = new Viterbi(model);
	}

	public TextTagger(List<PosSentence> sentences) throws Exception {
		model = ModelTrainer.execute(sentences);
   	    vit = new Viterbi(model);
	}

	public TextTagger(Model trainedModel) throws Exception {
		model = trainedModel;
   	    vit = new Viterbi(model);
	}

	/**
	 * Tag a collection of sentences.
	 * 
	 * @param sentence - PosSentence containing the words
	 * @throws Exception
	 */
	public void tag(List<PosSentence> sentences) throws Exception {
   	    vit.tag(sentences);
	}

	/**
	 * Tag a text retrieving only the valid sentences in a list.
	 * 
	 * @param sentence - PosSentence containing the words
	 * @return List<String>
	 * @throws Exception
	 */
	public List<String> tag(PosSentence sentence) throws Exception {
	
   	    List<String> words = new ArrayList<>();
   	    for (PosToken token : sentence.getTokens()) {
   	    	words.add(token.getName());
   	    }
   	    
   	    List<String> tags = vit.getMostLikelyTags(words);

   	    if (tags.size() != words.size()) {
   	    	throw new TaggerException("Number of tags different from number of tokens. Something went wrong");
   	    }
   	    
		for (int i = 0; i < sentence.getTokens().size(); i++) {
			PosToken token = sentence.getTokens().get(i);
			token.setTag(tags.get(i));
		}
		
		return tags;
	}
	
	/**
	 * Tag a text retrieving only the valid sentences in a list.
	 * 
	 * @param text - tag a single text
	 * @return List<PosToken>
	 * @throws Exception
	 */
	public List<PosToken> tag(String text) throws TaggerException {
	
   	    List<String> words = TextReader.getWordsFromUntaggedText(text);
   	    List<String> tags = vit.getMostLikelyTags(words);

   	    if (words.size() != tags.size()) {
   	    	throw new TaggerException("Number of tags (" + tags.size() + ") is different from number of words (" + words.size() + ")");
   	    }
   	    
   	    List<PosToken> tokens = new ArrayList<>();
   	    for (int i = 0; i < tags.size(); i++) {
			tokens.add(new PosToken(words.get(i), tags.get(i)));
		}
   	    
   	    return tokens;
	}
}
