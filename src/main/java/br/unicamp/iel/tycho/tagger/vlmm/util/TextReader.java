package br.unicamp.iel.tycho.tagger.vlmm.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Reader for raw text files.
 *
 * @author Fabio Kepler
 * @version 1.0 01/08/2013
 */
public class TextReader {

	public static final String UNTAGGED_TEXT_DELIM = " ";

	public TextReader() {
	}

	/**
	 * Process the text in a file or in the files inside a directory, converting them to a list of words (tokens).
	 * 
	 * @param dirName
	 * @return List<String>
	 * @throws Exception
	 */
	public List<List<String>> read(String name) throws Exception {
		
		File dirOrFile = new File(name);
		
		if (!dirOrFile.isDirectory()) {
			return read(dirOrFile);
		}
		
		List<List<String>> sentences = new ArrayList<>();
		File[] files = dirOrFile.listFiles();
		
		for (File file : files) {
			if (!file.isFile()) continue;
			
			try {
				sentences.addAll(read(file));
			} catch (Exception e) {
				System.out.println(file.getName() + "\n\n");
				e.printStackTrace();
			}
		}
		
		return sentences;
	}
	
	/**
	 * Process a single file.
	 * 
	 * @param fileName
	 * @return Text
	 * @throws Exception
	 */
	public List<List<String>> readFile(String fileName) throws Exception {
		return read(new File(fileName));
	}
	
	/**
	 * Read a raw text file and convert to sentences of words (tokens).
	 * 
	 * @param file
	 * @return String
	 * @throws Exception
	 */
	private List<List<String>> read(File file) throws Exception {
		
		List<String> lines = FileUtils.readLines(file, "UTF-8");
		
		return getSentences(getWordsFromUntaggedText(lines));
	}
	
	/**
	 * Make a list of sentences from the words from a text.
	 * 
	 * @param words list containing the words. Use one method to get the words like Functions.getWordsFromUntaggedText
	 * @return List<List<String>> list containing a list of words divided into sentences
	 * 
	 * @author Luiz Veronesi
	 * @version 1.0 11/10/2012
	 */
	public static List<List<String>> getSentences(List<String> words) {
		
		List<List<String>> sentences = new ArrayList<List<String>>();
		List<String> oneSentence = new ArrayList<String>();

		// Create sentences from the words informed
		for (String word : words) {
			if (ArrayUtils.contains(Functions.FINAL_PUNCTUATION_SYMBOLS, word)) {
				oneSentence.add(word);
				sentences.add(oneSentence);
				oneSentence = new ArrayList<String>();
	        } else {
	        	oneSentence.add(word);
	        }
	    }
		
		return sentences;
	}
	
	public static List<String> getWordsFromUntaggedText(String... lines) {
		List<String> list = new ArrayList<String>();
		Collections.addAll(list, lines);
		return getWordsFromUntaggedText(list);
	}
	
	/**
	 * Retrieve words from an untagged collection of words.
	 * 
	 * @param lines
	 * @return List<String>
	 */
	public static Vector<String> getWordsFromUntaggedText(List<String> lines) {
		
		Pattern r = Pattern.compile("\\p{Punct}", Pattern.CASE_INSENSITIVE);
		Vector<String> words = new Vector<String>();
		
		for (String line : lines) {
			
			String[] line_tokens = line.split(UNTAGGED_TEXT_DELIM);
			
			for (String token : line_tokens) {
				Matcher m = r.matcher(token);
				
			    if (m.find()) {
		    		String foundPunct = m.group();
		    		
		    		if (token.equals(foundPunct)) {
		    			words.add(foundPunct);
		    			continue;
		    		}
		    		
		    		// Added by Luiz Veronesi
		    		/*
		    		 * Sometimes two punctuation symbols are together as a symbol, like --.
		    		 * When this happens a java.util.NoSuchElementException is thrown.
		    		 * I will verify first if StringTokenizer has next element, if it
		    		 * doesn't, I will add to words the found punctuation symbol.
		    		 */
		    		
		    		StringTokenizer st = new StringTokenizer(token, foundPunct);
		    		
		    		if (st.hasMoreTokens()) {
		    			words.add(st.nextToken());
		    		}
		    		
		    		words.add(foundPunct);
		    		
			    } else {
			    	/*
			    	 * Added by Luiz Veronesi: ignores whitespace. 08/11/2012
			    	 */
			    	if (!StringUtils.isEmpty(token)) {
			    		words.add(token);
			    	}
			    }
			}
		}
		
		return words;
	}

}
