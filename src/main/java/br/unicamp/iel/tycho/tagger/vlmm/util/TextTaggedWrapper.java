package br.unicamp.iel.tycho.tagger.vlmm.util;

import java.util.List;

/**
 * Wrapper for the tagged text. Contains tagged text and tags and words separated.
 * 
 * @author Luiz Veronesi
 * @version 1.0 09/10/2012
 */
public class TextTaggedWrapper {

	private String taggedText;
	
	private List<String> words;
	
	private List<String> tags;

	public String getTaggedText() {
		return taggedText;
	}

	public void setTaggedText(String taggedText) {
		this.taggedText = taggedText;
	}

	public List<String> getWords() {
		return words;
	}

	public void setWords(List<String> words) {
		this.words = words;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}
}
