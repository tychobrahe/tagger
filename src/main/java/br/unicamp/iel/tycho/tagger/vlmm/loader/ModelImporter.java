package br.unicamp.iel.tycho.tagger.vlmm.loader;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;

import br.unicamp.iel.tycho.tagger.vlmm.model.AffixTree;
import br.unicamp.iel.tycho.tagger.vlmm.model.ContextTree;
import br.unicamp.iel.tycho.tagger.vlmm.model.MatrixAccumulator;
import br.unicamp.iel.tycho.tagger.vlmm.model.Model;
import br.unicamp.iel.tycho.tagger.vlmm.util.ModelOptions;

/**
 * Loads the model from an old trained file.
 * Original trained file was splitted into 4 files: words.data, tags.data, tree.data and affix_tree.data.
 *
 * @author Luiz Veronesi
 * @version 1.0 11/07/2013
 */
public class ModelImporter extends ModelFactory {

	/**
	 * Loads the model from the trained file.
	 * 
	 * @return
	 * @throws Exception
	 */
	public static Model execute() throws Exception {
		
		ModelImporter loader = new ModelImporter();
		
		Model model = new Model(new ModelOptions());
		model.setWordToTagCounter(loader.loadWords("data/words.data"));
		model.setTagToWordCounter(loader.loadTags("data/tags.data"));
		model.setLeftContextTree(loader.loadContextTree("data/tree.data"));
		model.setWordSuffixTree(loader.loadAffixTree("data/affix_tree.data"));

		loader.compute(model);
		
		return model;
	}
	
	/**
	 * Load words data from trained data file.
	 * 
	 * @return StateToStateMatrix
	 * @throws Exception
	 */
	private MatrixAccumulator loadWords(String filename) throws Exception {

		List<String> lines = FileUtils.readLines(new File(filename));

		MatrixAccumulator wordStates = new MatrixAccumulator();
		for (String line : lines) {
			
			String[] values = line.split(" ");
			
			String fromState = values[0];
			Double probability = new Double(values[1]);
			String toState = values[2];
			
			wordStates.add(fromState, toState, probability);
		}
		
		return wordStates;
	}
	
	/**
	 * Load tags data from trained data file.
	 * 
	 * @return StateToStateMatrix
	 * @throws Exception
	 */
	private MatrixAccumulator loadTags(String filename) throws Exception {
		
		List<String> lines = FileUtils.readLines(new File(filename));
		
		MatrixAccumulator tagWordProbabilities = new MatrixAccumulator();
		for (String line : lines) {
			
			String[] values = line.split(" ");
			
			String fromState = values[0];
			Double probability = new Double(values[1]);
			String toState = values[2];
			
			if (!probability.equals(0.0)  && !toState.isEmpty() && !fromState.isEmpty())
			tagWordProbabilities.add(fromState, toState, probability);
		}
		
		return tagWordProbabilities;
	}
	
    /**
     * Loads the affix tree.
     *  
     * @return AffixTree
     */
    private AffixTree loadAffixTree(String filename) throws Exception {
    	AffixTreeModelLoader loader = new AffixTreeModelLoader();
    	return loader.execute(filename);
    }
    
    /**
     * Loads the affix tree.
     *  
     * @return AffixTree
     */
    private ContextTree loadContextTree(String filename) throws Exception {
    	ContextTreeModelLoader loader = new ContextTreeModelLoader();
    	return loader.execute(filename);
    }
}
