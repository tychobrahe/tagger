package br.unicamp.iel.tycho.tagger.vlmm.loader;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;

import br.unicamp.iel.tycho.tagger.vlmm.model.AffixTree;
import br.unicamp.iel.tycho.tagger.vlmm.model.ContextTree;
import br.unicamp.iel.tycho.tagger.vlmm.model.Model;
import br.unicamp.iel.tycho.tagger.vlmm.pos.PosDocument;
import br.unicamp.iel.tycho.tagger.vlmm.pos.PosReader;
import br.unicamp.iel.tycho.tagger.vlmm.pos.PosSentence;
import br.unicamp.iel.tycho.tagger.vlmm.pos.PosToken;
import br.unicamp.iel.tycho.tagger.vlmm.util.ModelOptions;

/**
 * Responsible for training the model from POS tagged files.
 * 
 * @author Luiz Veronesi
 * @version 1.0 31/07/2013
 */
public class ModelTrainer extends ModelFactory {

    /**
     * Execute the model trainer reading the file or files specified and
     * generating a list of POS sentences.
     * 
     * @param inputFile
     *            - a directory or a single file path
     * @return Model - the trained model
     * @throws Exception
     */
    public static Model execute(String inputFile, Boolean tychoPattern) throws Exception {

        PosReader posReader = new PosReader(tychoPattern);
        List<PosDocument> documents = posReader.read(inputFile);

        List<PosSentence> sentences = new ArrayList<>();
        for (PosDocument document : documents) {
            sentences.addAll(document.getSentences());
        }

        ModelTrainer trainer = new ModelTrainer();
        return trainer.train(sentences);
    }

    /**
     * Execute the model trainer given the sentences.
     * 
     * @param inputFile
     *            - a directory or a single file path
     * @return Model - the trained model
     * @throws Exception
     */
    public static Model execute(List<PosSentence> sentences) throws Exception {
        ModelTrainer trainer = new ModelTrainer();
        return trainer.train(sentences);
    }

    /**
     * Starting model training based on the sentences.
     * 
     * @param sentences
     * @return
     */
    private Model train(List<PosSentence> sentences) {

        Model model = new Model(new ModelOptions());
        model.setInputSentencesSize(sentences.size());

        firstPassing(model, sentences);
        secondPassing(model, sentences);

        return model;
    }

    /**
     * Do the first passing creating the relations between word and tag,
     * building the affix tree and computing necessary values.
     * 
     * @param model
     * @param sentences
     * @return boolean
     */
    private boolean firstPassing(Model model, List<PosSentence> sentences) {

        createWordToTagCounter(model, sentences);

        compute(model);

        return true;
    }

    /**
     * Build the affix tree.
     * 
     * @param words
     * @param tags
     * @return
     */
    private void buildAffixTrees(Model model, List<PosSentence> sentences) {

        // Get suffixes from words for later handle of unknown words.
        System.out.println("Building suffix tree");

        AffixTree wordSuffixTree = model.getWordSuffixTree();

        for (PosSentence sentence : sentences) {
            for (PosToken token : sentence.getTokens()) {
                String word = token.getName();
                String tag = token.getTag();

                if (model.getWordCounter().get(word) <= model.getModelOptions().getUnknownWordThreshold()) {
                    wordSuffixTree.addBranch(tag, model.getWordSuffix(word));
                } else if (model.getOpenedTags().contains(tag)) {
                    wordSuffixTree.addBranch(tag, model.getWordSuffix(word));
                }
            }
        }

        // Add some special information about numbers to the affix trees.
        double maxCount = 0.0;
        String probableNumericalTag = "NUM";
        for (Entry<String, Double> entry : model.getNumericalTags().entrySet()) {
            if (entry.getValue() >= maxCount) {
                maxCount = entry.getValue();
                probableNumericalTag = entry.getKey();
            }
        }
        for (int i = 0; i < 10; i++) {
            wordSuffixTree.addBranch(probableNumericalTag, Integer.toString(i));
        }

        model.setWordSuffixTree(wordSuffixTree);
    }

    private void secondPassing(Model model, List<PosSentence> sentences) {

        buildAffixTrees(model, sentences);
        buildContextTree(model, sentences);

        AffixTree wordSuffixTree = model.getWordSuffixTree();
        wordSuffixTree.setName("Suffix");
        wordSuffixTree.setDensity(5);
        wordSuffixTree.setCutValue(0.0);
        wordSuffixTree.shake(false);
        wordSuffixTree.prune();
        wordSuffixTree.printStatistics();

        ContextTree leftContextTree = model.getLeftContextTree();
        leftContextTree.setName("Left");
        leftContextTree.setDensity(model.getModelOptions().getContextTreeDensity());
        leftContextTree.setCutValue(model.getModelOptions().getContextTreeCutValue());
        leftContextTree.setInputSize(model.getInputWordsSize());
        leftContextTree.setAverageSentenceSize((double) model.getInputWordsSize()
                / (double) model.getInputSentencesSize());
        leftContextTree.shake(false);
        leftContextTree.prune();
        leftContextTree.printStatistics();
    }

    private void buildContextTree(Model model, List<PosSentence> sentences) {

        for (PosSentence sentence : sentences) {
            Deque<String> context = new ArrayDeque<String>();
            for (PosToken token : sentence.getTokens()) {
                String tag = token.getTag();
                
                if (StringUtils.isEmpty(tag)) continue;
                
                model.getLeftContextTree().addBranch(tag, context);
                context.addFirst(tag);
                
                if (context.size() > model.getModelOptions().getContextTreeMaxOrder()) {
                    context.removeLast();
                }
            }
        }
    }

    /**
     * Create the list of words and tags and associate them. This is the first
     * step to build the affix tree.
     * 
     * @param words
     * @param tags
     * @return boolean
     */
    private void createWordToTagCounter(Model model, List<PosSentence> sentences) {

        for (PosSentence sentence : sentences) {
            for (PosToken token : sentence.getTokens()) {
                if (StringUtils.isEmpty(token.getName())) continue;
                
                model.getWordToTagCounter().increment(token.getName(), token.getTag());
                model.getTagToWordCounter().increment(token.getTag(), token.getName());
            }
        }
    }
}
