package br.unicamp.iel.tycho.tagger.vlmm.util;

/**
 * @author Fabio Kepler
 * @version 1.0 2005
 * 
 * @author Luiz Veronesi
 * @version 1.0 Set/2012
 * 
 * Modificados os nomes de todas as variaveis retirando o _ e criados os getters e setters. As variaveis sao privadas agora.
 * 
 * @author Fabio Kepler
 * @version 3.0 Dec/2012
 * 
 * Usando apenas atributos estritamente necessários.
 */
public class ModelOptions {

	private Double contextTreeCutValue; // ContextTree pruning constant.
	private Integer contextTreeDensity; // Only sequences of tags occurring at
										// least this number of times are put on
										// the tree.
	private Integer contextTreeMaxOrder;
	private Double wordSuffixSize = null;
	private Integer openedTagMinimumAssigns;
	private Integer unknownWordThreshold;

	public ModelOptions() {
		this.contextTreeCutValue = 50.0;
		this.openedTagMinimumAssigns = 10;
		this.unknownWordThreshold = 5;
		this.contextTreeDensity = 1;
		this.contextTreeMaxOrder = 5;
		this.wordSuffixSize = null; //11.0 / 20.0;
	}
	
	/**
	 * @return the contextTreeCutValue
	 */
	public Double getContextTreeCutValue() {
		return contextTreeCutValue;
	}

	/**
	 * @return the contextTreeDensity
	 */
	public Integer getContextTreeDensity() {
		return contextTreeDensity;
	}

	/**
	 * @return the contextTreeMaxOrder
	 */
	public Integer getContextTreeMaxOrder() {
		return contextTreeMaxOrder;
	}

	/**
	 * @return the wordSuffixSize
	 */
	public Double getWordSuffixSize() {
		return wordSuffixSize;
	}

	/**
	 * @return the openedTagMinimumAssigns
	 */
	public Integer getOpenedTagMinimumAssigns() {
		return openedTagMinimumAssigns;
	}

	/**
	 * @return the unknownWordThreshold
	 */
	public Integer getUnknownWordThreshold() {
		return unknownWordThreshold;
	}

	/**
	 * @param contextTreeCutValue
	 *            the contextTreeCutValue to set
	 */
	public void setContextTreeCutValue(Double contextTreeCutValue) {
		this.contextTreeCutValue = contextTreeCutValue;
	}

	/**
	 * @param contextTreeDensity
	 *            the contextTreeDensity to set
	 */
	public void setContextTreeDensity(Integer contextTreeDensity) {
		this.contextTreeDensity = contextTreeDensity;
	}

	/**
	 * @param contextTreeMaxOrder the contextTreeMaxOrder to set
	 */
	public void setContextTreeMaxOrder(Integer contextTreeMaxOrder) {
		this.contextTreeMaxOrder = contextTreeMaxOrder;
	}

	/**
	 * @param wordSuffixSize the wordSuffixSize to set
	 */
	public void setWordSuffixSize(Double wordSuffixSize) {
		this.wordSuffixSize = wordSuffixSize;
	}

	/**
	 * @param openedTagMinimumAssigns
	 *            the openedTagMinimumAssigns to set
	 */
	public void setOpenedTagMinimumAssigns(Integer openedTagMinimumAssigns) {
		this.openedTagMinimumAssigns = openedTagMinimumAssigns;
	}

	/**
	 * @param unknownWordThreshold
	 *            the unknownWordThreshold to set
	 */
	public void setUnknownWordThreshold(Integer unknownWordThreshold) {
		this.unknownWordThreshold = unknownWordThreshold;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ModelOptions [" + (contextTreeCutValue != null ? "contextTreeCutValue=" + contextTreeCutValue + ", " : "")
				+ (contextTreeDensity != null ? "contextTreeDensity=" + contextTreeDensity + ", " : "")
				+ (contextTreeMaxOrder != null ? "contextTreeMaxOrder=" + contextTreeMaxOrder + ", " : "")
				+ (wordSuffixSize != null ? "wordSuffixSize=" + wordSuffixSize + ", " : "")
				+ (openedTagMinimumAssigns != null ? "openedTagMinimumAssigns=" + openedTagMinimumAssigns + ", " : "")
				+ (unknownWordThreshold != null ? "unknownWordThreshold=" + unknownWordThreshold : "") + "]";
	}
}
