package br.unicamp.iel.tycho.tagger.vlmm.model;

import java.util.HashMap;

import org.apache.commons.math3.util.FastMath;

/**
 * StarTree counter.
 * 
 * @author Luiz Veronesi
 * @version 1.0 29/08/2012
 * 
 * @author Fabio N. Kepler
 * @version 3.0 24/06/2013
 */
public class MatrixAccumulator extends HashMap<String, Accumulator> {

    private static final long serialVersionUID = 1287599486608606871L;

    public Accumulator set(String key, String subKey, Double value) {
        return this.put(key, new Accumulator(subKey, value));
    }

    public Double increment(String key, String subKey) {
        if (this.containsKey(key)) {
            return this.get(key).increment(subKey);
        } else {
            this.put(key, new Accumulator(subKey, 1.0));
            return 0.0;
        }
    }

    public Accumulator add(String key, String subKey, Double value) {
        Accumulator acc = null;
        if (this.containsKey(key)) {
            acc = this.get(key);
            acc.add(subKey, value);
        } else {
            acc = new Accumulator(subKey, value);
            this.put(key, acc);
        }
        return acc;
    }

    public Double get(Object key, Object subKey) {
        Accumulator acc = super.get(key);
        if (acc == null) {
            return 0.0;
        } else {
            return acc.get(subKey);
        }
    }

    public Double getLog(Object key, Object subKey) {
        return FastMath.log(this.get(key, subKey)); // Zero is handled
    }

}
