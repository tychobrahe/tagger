package br.unicamp.iel.tycho.tagger.vlmm.model;

import gnu.trove.map.hash.THashMap;
import gnu.trove.set.hash.THashSet;

import java.util.ArrayList;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.util.FastMath;

import br.unicamp.iel.tycho.tagger.vlmm.util.ModelOptions;

/**
 * @author Fabio Kepler
 * @version 1.0 unknown
 * 
 * @author Luiz Veronesi
 * @version 2.0 Set/2012
 * 
 *          Basicamente o c�digo foi traduzido de C++ para Java, substituindo
 *          alguns tipos conforme documenta��o. Cada linha em C++ foi comentada
 *          e logo abaixo a vers�o em Java. Algumas linhas foram modificadas em
 *          bloco.
 * 
 *          Os seguintes m�todos utilizado por AlgoViterbi foram exclu�dos e/ou
 *          substitu�dos e parte da l�gica conferida a eles transferida para a
 *          classe que realiza a chamada:
 * 
 *          inline string tagState(tags_iterator it_tag) {return
 *          (*it_tag).first;}; Este m�todo retorna a chave do elemento do
 *          iterator, no caso atual a String do StringCounter. Em um loop de
 *          Map.Entry, o m�todo getKey j� realiza este procedimento.
 * 
 *          inline tags_iterator tagStatesBegin() {return
 *          m_tag_counter.begin();}; inline tags_iterator tagStatesEnd() {return
 *          m_tag_counter.end();};
 * 
 *          Troquei estes dois m�todos para o m�todo getTagStates que retorna o
 *          m_tag_counter;
 * 
 * @author Fabio Kepler
 * @version 3.0
 * 
 *          Major revamping.
 */
public class Model {
	
	private ContextTree leftContextTree = new ContextTree();
	private AffixTree wordSuffixTree = new AffixTree();

	private Integer inputWordsSize;
	private Integer inputTagsSize;
	
	/**
	 * Contains the number of sentences used as input. It is used to train the model at
	 * the second round.
	 */
	private Integer inputSentencesSize;
	
	private Double averageOpenWordLength;

	private Accumulator tagCounter;
	private Accumulator wordCounter;
	private MatrixAccumulator wordToTagCounter;
	private MatrixAccumulator tagToWordCounter = new MatrixAccumulator();

	private List<String> closedTags = new ArrayList<String>();
	
	/**
	 * Opened tags are associated tags with many different words.
	 */
	private Set<String> openedTags = new THashSet<String>();

	private Accumulator unknownTagCounter;

	private Set<String> ambiguousWords;
	private Map<Integer, Set<String>> wordsAmbiguity = new THashMap<Integer, Set<String>>();

	// Tags occurring with words where the first letter is in uppercase.
	private Accumulator uppercaseLetterTags; 
											
	// Tags occurring with words that have all letters in uppercase.
	private Accumulator uppercaseWordTags; 
											
	// Tags occurring with words that start with a digit.
	private Accumulator numericalTags = new Accumulator(); 
															
	private Accumulator hyphenatedWordTags;

	private ModelOptions modelOptions;

	public Model(ModelOptions modelOptions) {
		this.setModelOptions(modelOptions);
	}

	/**
	 * @return the modelOptions
	 */
	public ModelOptions getModelOptions() {
		return modelOptions;
	}

	/**
	 * @param modelOptions
	 *            the modelOptions to set
	 */
	public void setModelOptions(ModelOptions modelOptions) {
		this.modelOptions = modelOptions;
		leftContextTree.setCutValue(modelOptions.getContextTreeCutValue());
		leftContextTree.setDensity(modelOptions.getContextTreeDensity());
	}


	public Set<String> getKnownWords() {
		return wordCounter.keySet();
	}

	public Set<String> getAmbiguousWords() {
		
		if (ambiguousWords == null) {
			ambiguousWords = new HashSet<>();
		}
		
		return ambiguousWords;
	}

	/**
	 * @return the inputSentencesSize
	 */
	public Integer getInputSentencesSize() {
		return inputSentencesSize;
	}

	/**
	 * @param inputSentencesSize
	 *            the inputSentencesSize to set
	 */
	public void setInputSentencesSize(Integer inputSentencesSize) {
		this.inputSentencesSize = inputSentencesSize;
	}

	public ContextTree getLeftContextTree() {
		return leftContextTree;
	}

	public void setLeftContextTree(ContextTree leftContextTree) {
		this.leftContextTree = leftContextTree;
	}

	public AffixTree getWordSuffixTree() {
		return wordSuffixTree;
	}

	public void setWordSuffixTree(AffixTree wordSuffixTree) {
		this.wordSuffixTree = wordSuffixTree;
	}

	public Integer getInputWordsSize() {
		return inputWordsSize;
	}

	public void setInputWordsSize(Integer inputWordsSize) {
		this.inputWordsSize = inputWordsSize;
	}

	public Integer getInputTagsSize() {
		return inputTagsSize;
	}

	public void setInputTagsSize(Integer inputTagsSize) {
		this.inputTagsSize = inputTagsSize;
	}

	public Double getAverageOpenWordLength() {
		return averageOpenWordLength;
	}

	public void setAverageOpenWordLength(Double averageOpenWordLength) {
		this.averageOpenWordLength = averageOpenWordLength;
	}

	public Accumulator getTagCounter() {
		
		if (tagCounter == null) {
			tagCounter = new Accumulator();
		}
		
		return tagCounter;
	}

	public void setTagCounter(Accumulator tagCounter) {
		this.tagCounter = tagCounter;
	}

	public Accumulator getWordCounter() {
		
		if (wordCounter == null) {
			wordCounter = new Accumulator();
		}
		
		return wordCounter;
	}

	public void setWordCounter(Accumulator wordCounter) {
		this.wordCounter = wordCounter;
	}

	public MatrixAccumulator getWordToTagCounter() {
		
		if (wordToTagCounter == null) {
			wordToTagCounter = new MatrixAccumulator();
		}
		
		return wordToTagCounter;
	}

	public void setWordToTagCounter(MatrixAccumulator wordToTagCounter) {
		this.wordToTagCounter = wordToTagCounter;
	}

	public MatrixAccumulator getTagToWordCounter() {
		return tagToWordCounter;
	}

	public void setTagToWordCounter(MatrixAccumulator tagToWordCounter) {
		this.tagToWordCounter = tagToWordCounter;
	}

	public List<String> getClosedTags() {
		return closedTags;
	}

	public void setClosedTags(List<String> closedTags) {
		this.closedTags = closedTags;
	}

	public Set<String> getOpenedTags() {
		return openedTags;
	}

	public void setOpenedTags(Set<String> openedTags) {
		this.openedTags = openedTags;
	}

	public Accumulator getUnknownTagCounter() {
		
		if (unknownTagCounter == null) {
			unknownTagCounter = new Accumulator();
		}
		
		return unknownTagCounter;
	}

	public void setUnknownTagCounter(Accumulator unknownTagCounter) {
		this.unknownTagCounter = unknownTagCounter;
	}

	public Map<Integer, Set<String>> getWordsAmbiguity() {
		return wordsAmbiguity;
	}

	public void setWordsAmbiguity(Map<Integer, Set<String>> wordsAmbiguity) {
		this.wordsAmbiguity = wordsAmbiguity;
	}

	public Accumulator getUppercaseLetterTags() {
		
		if (uppercaseLetterTags == null) {
			uppercaseLetterTags = new Accumulator();
		}
		
		return uppercaseLetterTags;
	}

	public void setUppercaseLetterTags(Accumulator uppercaseLetterTags) {
		this.uppercaseLetterTags = uppercaseLetterTags;
	}

	public Accumulator getUppercaseWordTags() {

		if (uppercaseWordTags == null) {
			uppercaseWordTags = new Accumulator();
		}
		
		return uppercaseWordTags;
	}

	public void setUppercaseWordTags(Accumulator uppercaseWordTags) {
		this.uppercaseWordTags = uppercaseWordTags;
	}

	public Accumulator getNumericalTags() {
		return numericalTags;
	}

	public void setNumericalTags(Accumulator numericalTags) {
		this.numericalTags = numericalTags;
	}

	public Accumulator getHyphenatedWordTags() {

		if (hyphenatedWordTags == null) {
			hyphenatedWordTags = new Accumulator();
		}

		return hyphenatedWordTags;
	}

	public void setHyphenatedWordTags(Accumulator hyphenatedWordTags) {
		this.hyphenatedWordTags = hyphenatedWordTags;
	}

	public void setAmbiguousWords(Set<String> ambiguousWords) {
		this.ambiguousWords = ambiguousWords;
	}
	
	public Set<String> getCandidateTagsForWord(String word) {
		Set<String> tags = new THashSet<String>();

		if (!this.isWordKnown(word)) {
			Set<String> suffixTags = this.getWordSuffixTree().getSproutsOfBranch(getWordSuffix(word));
			if (suffixTags.isEmpty()) {
				tags = this.getOpenedTags();
			} else {
				tags = suffixTags;
			}
		} else {
			for (Map.Entry<String, Double> itTrans : this.getWordToTagCounter().get(word).entrySet()) {
				tags.add(itTrans.getKey());
			}
		}

		return tags;
	}
	
	public boolean isWordKnown(String word) {
		return this.getWordCounter().containsKey(word);
		// && getNumberOfWordOccurences(word) >
		// algoParam.unknown_word_threshold);
	}
	
	public boolean isWordAmbiguous(String word) {
		return this.ambiguousWords.contains(word);
	}
	
	public String getWordSuffix(String word) {
	    //setAverageOpenWordLength(7.0);//(8.09772);
	    
		int suffixSize;
		String suffix = "";

		if (word.length() > 2) {
			if (this.getModelOptions().getWordSuffixSize() != null) {
				suffixSize = (int) Math.floor((word.length() * this.getModelOptions().getWordSuffixSize()) + 0.5);
			} else {
				suffixSize = (word.length() > this.getAverageOpenWordLength()) ? word.length() * 11 / 20 : word.length() / 2;
			}
			
			if (suffixSize == 0)
				suffixSize = 1;
			if (word.length() < suffixSize) {
				System.out.println("Warning!: word '" + word
						+ "' is shorter than its suffix! " + word.length()
						+ " vs. " + suffixSize);
			} else {
				suffix = word.substring(word.length() - suffixSize, word.length());
				suffix = StringUtils.reverse(suffix);
			}
		} else {
			suffix = word;
		}
		
		return suffix;
	}
	
	public Double getLogProbabilityAndContextUsed(String candidateTag, String word, List<String> leftContext, Deque<String> contextUsed) {
		
		Double wordProb = 0.0;
		if (this.isWordKnown(word)) {
			wordProb = this.getKnownWordLogProbability(candidateTag, word);
		} else {
			wordProb = this.getUnknownWordLogProbability(candidateTag, word);
		}

		Double leftProb = 0.0;
		leftProb = this.getLeftContextTree().getSproutLogProbability(candidateTag, leftContext, contextUsed);
		
		if (leftProb == Double.NEGATIVE_INFINITY) {
			if (this.getTagCounter().containsKey(candidateTag)) {
				leftProb = this.getTagCounter().getLog(candidateTag) - this.getTagCounter().getTotalLog(); // or - with inputTagsSize
			} else {
				System.err.println("Warning: unknown tag" + candidateTag
						+ " for word '" + word + "' and left context: "
						+ leftContext);
			}
		}
		
//		if (word.equals("supremo")) {
//		    System.err.println(word + "/" + candidateTag + ": Pw: " + wordProb + "; Pl: " + leftProb);
//		}
		
		return wordProb + leftProb;
	}
	
	private Double getKnownWordLogProbability(String candidateTag, String word) {
		
		if (this.getTagToWordCounter().get(candidateTag).containsKey(word)) {
			return this.getTagToWordCounter().getLog(candidateTag, word) - this.getTagToWordCounter().get(candidateTag).getTotalLog();
		} else {
			return Double.NEGATIVE_INFINITY;
		}
	}

	private Double getUnknownWordLogProbability(String candidateTag, String word) {
		
		Double case1Prob = Double.NEGATIVE_INFINITY;
		Double case2Prob = Double.NEGATIVE_INFINITY;
		Double hyphenProb = Double.NEGATIVE_INFINITY;
		Double suffixProb = Double.NEGATIVE_INFINITY;

		if (this.hasUpperChar(word)) {
			if (Character.isUpperCase(word.charAt(0))
					&& Character.isUpperCase(word.charAt(word.length() - 1))) {
				case1Prob = this.getUppercaseWordTags().getLog(candidateTag);
			}
			case2Prob = this.getUppercaseLetterTags().getLog(candidateTag);
		}
		
		if (word.length() > 2 && word.contains("-")) {
			hyphenProb = this.getHyphenatedWordTags().getLog(candidateTag);
		}

		suffixProb = this.getWordSuffixTree().getSproutCount(candidateTag, this.getWordSuffix(word));
		
		suffixProb = FastMath.log(suffixProb);

		Double logProb = 0.0;
		
		if (case1Prob > Double.NEGATIVE_INFINITY) logProb += case1Prob;
		if (case2Prob > Double.NEGATIVE_INFINITY) logProb += case2Prob;
		if (hyphenProb > Double.NEGATIVE_INFINITY) logProb += hyphenProb;
		if (suffixProb > Double.NEGATIVE_INFINITY) logProb += suffixProb;
		
//	    if (word.equals("supremo")) {
//	        System.err.println("Word: " + word + ", tag: " + candidateTag + " ; " + case1Prob + "; " + hyphenProb + "; " + suffixProb + "; " + case2Prob + "; suff: " + this.getWordSuffix(word));
//	        System.err.println("sprout count: " + this.getWordSuffixTree().getSproutCount(candidateTag, this.getWordSuffix(word)));
//	    }
		
		return logProb;
	}
	
	public boolean hasUpperChar(String word) {
		for (int c = 0; c < word.length(); c++) {
			if (Character.isUpperCase(word.charAt(c))) {
				return true;
			}
		}
		return false;
	}

}
