/**
 * 
 */
package br.unicamp.iel.tycho.tagger.vlmm.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author Kepler
 * 
 */
public class MapUtils {

    /**
     * Returns a map with keys sorted by value.
     * 
     * Adapted from various solutions at http://stackoverflow.com/questions/109383/how-to-sort-a-mapkey-value-on-the-values-in-java
     * 
     * @author Kepler
     * 
     * @param map to be sorted
     * @return a map sorted by values
     */
    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue2(Map<K, V> map) {
        @SuppressWarnings("unchecked")
        // Might use Collections or Arrays
        //List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(map.entrySet());
        //Collections.sort(list, new ByValue<K, V>());
        // Using Arrays is more efficient (it's used internally by Collections.sort).
        Map.Entry<K, V>[] array = map.entrySet().toArray(new Map.Entry[map.size()]);
        Arrays.sort(array, new ByValue<K, V>());

        Map<K, V> result = new LinkedHashMap<K, V>();
        for (Map.Entry<K, V> entry : array)
            result.put(entry.getKey(), entry.getValue());

        return result;
    }

    /**
     * Returns a list of map entries sorted by value.
     * 
     * Adapted from various solutions at http://stackoverflow.com/questions/109383/how-to-sort-a-mapkey-value-on-the-values-in-java
     * 
     * @author Kepler
     * 
     * @param map to be sorted
     * @return list of entries sorted by values
     */
    public static <K, V extends Comparable<? super V>> List<Entry<K, V>> entriesSortedByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> entries = new ArrayList<Map.Entry<K, V>>(map.entrySet());
        Collections.sort(entries, new ByValue<K, V>());
        return entries;
    }
    
    public static <K, V extends Comparable<? super V>> List<Entry<K, V>> entriesSortedByValueDescending(Map<K, V> map) {
        List<Map.Entry<K, V>> entries = new ArrayList<Map.Entry<K, V>>(map.entrySet());
        Collections.sort(entries, Collections.reverseOrder(new ByValue<K, V>()));
        return entries;
    }


    private static class ByValue<K, V extends Comparable<? super V>> implements Comparator<Map.Entry<K, V>> {
        public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
            return o1.getValue().compareTo(o2.getValue());
        }
    }

}
