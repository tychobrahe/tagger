package br.unicamp.iel.tycho.tagger.vlmm.loader;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import br.unicamp.iel.tycho.tagger.vlmm.model.Accumulator;
import br.unicamp.iel.tycho.tagger.vlmm.model.Model;
import gnu.trove.set.hash.THashSet;

/**
 * This factory is used to build the Model. It contains all common methods used
 * by the trainer and the loader. These methods sets variables at the model
 * based on the list of words and tags.
 * 
 * @author Luiz Veronesi
 * @version 1.0 11/07/2013
 */
public class ModelFactory {

    public static final String DEFAULT_CONTEXT_FILE = "context.data";

    public static final String DEFAULT_AFFIX_FILE = "affix.data";

    public static final String DEFAULT_WORDTAG_FILE = "wordtag.data";

    /**
     * Main method. realizes the operations for computing variables for Model.
     * 
     * @param model
     */
    public void compute(Model model) {
        computeWordsAndTagsCounts(model);
        computeAmbiguity(model);
        computeOpenAndClosedTags(model);
        computeUnknownTags(model);
        computeUnknownWordFeaturesCounts(model);
    }

    /**
     * This method set opened tags, closed tags and calculate the average opened
     * words length.
     * 
     * Set all these variables at the Model.
     */
    private void computeOpenAndClosedTags(Model model) {

        int sumLengthWords = 0;
        int numOpenedWords = 0;

        // Consider a tag as an opened tag if it occurs in the corpus with more
        // than a predefined number of different words.
//        int num_opened = 0, num_closed = 0;
        for (Entry<String, Accumulator> entry : model.getTagToWordCounter().entrySet()) {
            String tag = entry.getKey();
            Accumulator words = entry.getValue();

            if (words.size() >= model.getModelOptions().getOpenedTagMinimumAssigns()) {
                model.getOpenedTags().add(tag);
                numOpenedWords += words.size();
//                num_opened++;
                for (Entry<String, Double> entry2 : words.entrySet()) {
                    sumLengthWords += entry2.getKey().length();
                }
            } else {
                model.getClosedTags().add(tag);
//                num_closed++;
            }
        }
//        System.err.println("opened: " + num_opened + "; num_closed: " + num_closed + "; open_words: " + numOpenedWords + "; sum length: " + sumLengthWords);

        model.setAverageOpenWordLength((double) 8.0);
        if (numOpenedWords > 0)
            model.setAverageOpenWordLength((double) (sumLengthWords / numOpenedWords));
        else
            model.setAverageOpenWordLength(0.0);
    }

    /**
     * Compute ambiguous data.
     * 
     * Sets words ambiguity and ambiguous words from the word to tag counter.
     * 
     * @param model
     */
    private void computeAmbiguity(Model model) {

        for (Entry<String, Accumulator> entry : model.getWordToTagCounter().entrySet()) {
            if (entry.getValue().size() > 1) { // Word is ambiguous
                model.getAmbiguousWords().add(entry.getKey());
                if (model.getWordsAmbiguity().containsKey(entry.getValue().size())) {
                    model.getWordsAmbiguity().get(entry.getValue().size()).add(entry.getKey());

                } else {
                    Set<String> newset = new THashSet<String>();
                    newset.add(entry.getKey());
                    model.getWordsAmbiguity().put(entry.getValue().size(), newset);
                }
            }
        }
    }

    /**
     * Compute words and tags counter.
     * 
     * @param words
     * @param tags
     */
    private void computeWordsAndTagsCounts(Model model) {

        for (Map.Entry<String, Accumulator> entry : model.getWordToTagCounter().entrySet()) {
            model.getWordCounter().add(entry.getKey(), entry.getValue().getTotal());

            for (Map.Entry<String, Double> tagEntry : entry.getValue().entrySet()) {
                model.getTagCounter().add(tagEntry.getKey(), tagEntry.getValue());
            }
        }

        model.setInputWordsSize(model.getWordCounter().size());
        model.setInputTagsSize(model.getTagCounter().size());
    }

    private boolean hasUpperCaseCharacter(String str)
    {
        for (Character c : str.toCharArray()) {
            if (Character.isUpperCase(c)) return true;
        }
        return false;
    };

    /**
     * Check words and set numerical, uppercase and hyphenated tags. To be
     * checked the tag must be greater than or equal to a specified threshold
     * value defined at model options.
     * 
     * @param words
     * @param tags
     */
    private void computeUnknownWordFeaturesCounts(Model model) {

        int threshold = model.getModelOptions().getOpenedTagMinimumAssigns();

        for (Map.Entry<String, Accumulator> entry : model.getWordToTagCounter().entrySet()) {

            String word = entry.getKey();

            //if (word.matches("[1-9].*")) {
            if (Character.isDigit(word.charAt(0))) {
                for (Map.Entry<String, Double> tagEntry : entry.getValue().entrySet()) {
//                    if (tagEntry.getValue() >= threshold) {
                        model.getNumericalTags().increment(tagEntry.getKey());
//                    }
                }
            }

            if (hasUpperCaseCharacter(word)) {
                if (Character.isUpperCase(word.charAt(0)) && Character.isUpperCase(word.charAt(word.length()-1))) {
                    for (Map.Entry<String, Double> tagEntry : entry.getValue().entrySet()) {
//                        if (tagEntry.getValue() >= threshold) {
                            model.getUppercaseWordTags().increment(tagEntry.getKey());
//                        }
                    }
                } else {
                    for (Map.Entry<String, Double> tagEntry : entry.getValue().entrySet()) {
//                        if (tagEntry.getValue() >= threshold) {
                            model.getUppercaseLetterTags().increment(tagEntry.getKey());
//                        }
                    }
                }
            }
//            if (StringUtils.isAllUpperCase(word)) {
//                for (Map.Entry<String, Double> tagEntry : entry.getValue().entrySet()) {
//                    if (tagEntry.getValue() >= threshold) {
//                        model.getUppercaseWordTags().increment(tagEntry.getKey());
//                    }
//                }
//            } else if (Character.isUpperCase(word.charAt(0))) {
//                for (Map.Entry<String, Double> tagEntry : entry.getValue().entrySet()) {
//                    if (tagEntry.getValue() >= threshold) {
//                        model.getUppercaseLetterTags().increment(tagEntry.getKey());
//                    }
//                }
//            }

            if (word.length() > 2 && word.contains("-")) {
                for (Map.Entry<String, Double> tagEntry : entry.getValue().entrySet()) {
//                    if (tagEntry.getValue() >= threshold) {
                        model.getHyphenatedWordTags().increment(tagEntry.getKey());
//                    }
                }
            }
        }
        
        for (Iterator<Entry<String, Double>> iterator = model.getNumericalTags().entrySet().iterator(); iterator
                .hasNext();) {
            Entry<String, Double> entry = iterator.next();
            if (entry.getValue() < threshold) {
                iterator.remove();
            }
        }
        for (Iterator<Entry<String, Double>> iterator = model.getUppercaseWordTags().entrySet().iterator(); iterator
                .hasNext();) {
            Entry<String, Double> entry = iterator.next();
            if (entry.getValue() < threshold) {
                iterator.remove();
            }
        }
        for (Iterator<Entry<String, Double>> iterator = model.getUppercaseLetterTags().entrySet().iterator(); iterator
                .hasNext();) {
            Entry<String, Double> entry = iterator.next();
            if (entry.getValue() < threshold) {
                iterator.remove();
            }
        }
        for (Iterator<Entry<String, Double>> iterator = model.getHyphenatedWordTags().entrySet().iterator(); iterator
                .hasNext();) {
            Entry<String, Double> entry = iterator.next();
            if (entry.getValue() < threshold) {
                iterator.remove();
            }
        }
    }

    /**
     * Creates the unknown tag counter.
     * 
     * @param words
     * @param tags
     * @return
     */
    private void computeUnknownTags(Model model) {

        for (Map.Entry<String, Accumulator> entry : model.getWordToTagCounter().entrySet()) {
            for (Map.Entry<String, Double> tagEntry : entry.getValue().entrySet()) {

                String word = entry.getKey();
                String tag = tagEntry.getKey();

                if (model.getWordCounter().get(word) <= model.getModelOptions().getUnknownWordThreshold()) {
                    model.getUnknownTagCounter().increment(tag);
                }
            }
        }
    }
}
