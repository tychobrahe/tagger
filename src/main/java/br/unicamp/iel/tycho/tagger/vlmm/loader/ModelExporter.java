package br.unicamp.iel.tycho.tagger.vlmm.loader;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SerializationUtils;

import br.unicamp.iel.tycho.tagger.vlmm.model.Accumulator;
import br.unicamp.iel.tycho.tagger.vlmm.model.AffixTree;
import br.unicamp.iel.tycho.tagger.vlmm.model.ContextTree;
import br.unicamp.iel.tycho.tagger.vlmm.model.MatrixAccumulator;
import br.unicamp.iel.tycho.tagger.vlmm.model.Model;
import br.unicamp.iel.tycho.tagger.vlmm.pos.PosToken;
import br.unicamp.iel.tycho.tagger.vlmm.util.ZipUtils;

/**
 * Exports the model.
 * All data can be saved at database or saved as files. Information will be split into 3 main structures:
 * 1- Word and tag probabilities information: file will contain the word, tag, word to tag probability 
 * and tag to word probability.
 * 2- ContextTree: the context tree will be serialized. 
 * 3- AffixTree: the context tree will be serialized.
 *
 * @author Luiz Veronesi
 * @version 1.0 01/08/2013
 */
public class ModelExporter extends ModelFactory {

	public static void main(String[] args) throws Exception {
	    String inputDir = "C:/work/tycho-git/framework/docs/pos/";
		String outputFile = "c:/temp/model.zip";
		
		Model model = ModelTrainer.execute(inputDir, Boolean.TRUE);
		
		ModelExporter exporter = new ModelExporter();
		exporter.execute(model, outputFile);
	}
	
	/**
	 * Execute the exporting generating a zip file with the specified output name 
	 * containing the context and affix trees and the word/tag probability files 
	 * based on the given model.
	 * 
	 * @param model
	 * @param outputFile
	 * @throws IOException
	 */
	public void execute(Model model, String outputFile) throws IOException {

		String tmpDir = FileUtils.getTempDirectory().getPath() + File.separator;
		
		this.writeWordTagData(tmpDir + DEFAULT_WORDTAG_FILE, model.getWordToTagCounter());
		this.writeContextTree(tmpDir + DEFAULT_CONTEXT_FILE, model.getLeftContextTree());
		this.writeAffixTree(tmpDir + DEFAULT_AFFIX_FILE, model.getWordSuffixTree());
		
		ZipUtils.compress(outputFile, tmpDir, DEFAULT_WORDTAG_FILE, DEFAULT_CONTEXT_FILE, DEFAULT_AFFIX_FILE);
	}
	
	/**
	 * Creates a file containing the pair word/tag and its probability in the format: word|tag|probability
	 * 
	 * @param outputFile
	 * @param wordToTagCounter
	 * @throws IOException
	 */
	private void writeWordTagData(String outputFile, MatrixAccumulator wordToTagCounter) throws IOException {
		
		List<PosToken> tokens = new ArrayList<>();
		
		for (Map.Entry<String, Accumulator> wordEntry : wordToTagCounter.entrySet()) {
			
			String word = wordEntry.getKey();
			
			for (Map.Entry<String, Double> tagForWordEntry : wordEntry.getValue().entrySet()) {
				
				String tag = tagForWordEntry.getKey();
				Double prob = tagForWordEntry.getValue();
				tokens.add(new PosToken(word, tag, prob));
			}
		}
		
		FileUtils.writeLines(new File(outputFile), tokens);
	}
	
	/**
	 * Serializes and writes context tree to an output file.
	 * 
	 * @param outputFile
	 * @param contextTree
	 * @throws IOException
	 */
	private void writeContextTree(String outputFile, ContextTree contextTree) throws IOException {
		byte[] data = SerializationUtils.serialize(contextTree);
		FileUtils.writeByteArrayToFile(new File(outputFile), data);
	}
	
	/**
	 * Serializes and writes affix tree to an output file.
	 * 
	 * @param outputFile
	 * @param affixTree
	 */
	private void writeAffixTree(String outputFile, AffixTree affixTree) throws IOException {
		byte[] data = SerializationUtils.serialize(affixTree);
		FileUtils.writeByteArrayToFile(new File(outputFile), data);
	}
}
