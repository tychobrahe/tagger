package br.unicamp.iel.tycho.tagger.vlmm.viterbi;

import java.util.ArrayList;
import java.util.List;

/**
 * SubPath para AlgoViterbi.
 * 
 * @author Luiz Veronesi
 * @version 1.0 05/09/2012
 * 
 * @author Fabio Kepler
 * @version 3.0 2013
 * 
 *          Using log probability. Only attribute and method names were changed
 *          here, in order to reflect the expected behavior of callers.
 */
public class Subpath {

	private List<String> tagSequence;

	private Double logProbability;

	public Subpath() {
		this.tagSequence = new ArrayList<String>();
	}

	public Subpath(Double logProbability) {
		this.tagSequence = new ArrayList<String>();
		this.logProbability = logProbability;
	}

	public Subpath(List<String> tagSequence, Double logProbability) {
		this.tagSequence = tagSequence;
		this.logProbability = logProbability;
	}

	public List<String> getTagSequence() {
		return tagSequence;
	}

	public void setTagSequence(List<String> tagSequence) {
		this.tagSequence = tagSequence;
	}

	public Double getLogProbability() {
		return logProbability;
	}

	public void setLogProbability(Double logProbability) {
		this.logProbability = logProbability;
	}

	public String toString() {
		return this.tagSequence.toString() + ";" + this.logProbability;
	}
}
