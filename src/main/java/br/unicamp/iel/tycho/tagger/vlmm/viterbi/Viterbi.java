package br.unicamp.iel.tycho.tagger.vlmm.viterbi;

import gnu.trove.map.hash.THashMap;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import br.unicamp.iel.tycho.tagger.vlmm.model.Model;
import br.unicamp.iel.tycho.tagger.vlmm.pos.PosDocument;
import br.unicamp.iel.tycho.tagger.vlmm.pos.PosSentence;
import br.unicamp.iel.tycho.tagger.vlmm.pos.PosToken;
import br.unicamp.iel.tycho.tagger.vlmm.util.TaggerException;

/**
 * Class that returns the most probable tags associated with a sequence of
 * words. This class uses Model to do that calculus.
 * 
 * @author Fabio Kepler
 * @version 1.0
 * 
 * @author Luiz Veronesi
 * @version 2.0 Removed print method, excluded variables m_best_path and
 *          m_putput, they are not necessary. Instead I modified sigmaFunction
 *          to receive sentence as a parameter. Renamed variables to Java
 *          pattern as specified by conventions.
 * 
 * @author Fabio Kepler
 * @version 3.0 Major refactoring and cleaning up.
 */
public class Viterbi {

	private Model model;

	public Viterbi(Model model) {
		this.model = model;
	}

	/**
	 * Tag the words of a document.
	 */
	public void tag(PosDocument document) throws TaggerException {
		tag(document.getSentences());
	}

	/**
	 * Set the sequences of tags with maximum probability given sequences of words.
	 */
	public void tag(List<PosSentence> sentences) throws TaggerException {
		for (PosSentence sentence : sentences) {
			tag(sentence);
		}
	}
	
	/**
	 * Set the sequence of tags with maximum probability given a sequence of words.
	 */
	public void tag(PosSentence sentence) throws TaggerException {
   	    List<String> words = new ArrayList<>();
   	    for (PosToken token : sentence.getTokens()) {
   	    	words.add(token.getName());
   	    }
   	    
   	    List<String> tags = getMostLikelyTags(words);
   	    
   	    List<PosToken> tokens = new ArrayList<>();
   	    for (int i = 0; i < words.size(); i++) {
   	    	tokens.add(new PosToken(words.get(i), tags.get(i)));
   	    }
   	    sentence.setTokens(tokens);
	}

	/**
	 * Return the sequence of tags with maximum probability given words. Uses the Viterbi algorithm.
	 */
	public List<List<String>> getTags(List<List<String>> sentences) throws TaggerException {
		List<List<String>> tags = new ArrayList<List<String>>();
		for (List<String> sentence : sentences) {
			tags.add(getMostLikelyTags(sentence));
		}
		return tags;
	}
	
	/**
	 * Return the sequence of tags with maximum probability given words. Uses the Viterbi algorithm.
	 */
	public List<String> getMostLikelyTags(List<String> words) throws TaggerException {
		
		if (words.isEmpty()) throw new TaggerException("No words given.");

		// Best subpaths found during evaluation
		Map<String, Map<String, Subpath>> bestSubpaths = new THashMap<String, Map<String, Subpath>>();
		
		Deque<Subpath> previousPaths = new ArrayDeque<Subpath>();
		previousPaths.add(new Subpath(0.0));

		for (String word : words) {
			
			this.calculateBestSubpaths(word, previousPaths, bestSubpaths);
			
			// TODO: adjust variables name here
			Deque<Subpath> nextPaths = new ArrayDeque<Subpath>();
			for (Entry<String, Map<String, Subpath>> entry : bestSubpaths.entrySet()) {
				for (Entry<String, Subpath> entry2 : entry.getValue().entrySet()) {
					String tag = entry.getKey();
				    Subpath path = new Subpath();
				    path.setLogProbability(entry2.getValue().getLogProbability());
				    ArrayList<String> sequence = new ArrayList<String>(entry2.getValue().getTagSequence());
				    path.setTagSequence(sequence);
				    path.getTagSequence().add(0, tag);
				    nextPaths.add(path);
				}
			}
			
			previousPaths.clear();
			previousPaths = nextPaths;
			bestSubpaths.clear();
		}

		Double maxProb = Double.NEGATIVE_INFINITY;
		List<String> maxSequence = null;
		for (Subpath path : previousPaths) {
			Double pathProb = path.getLogProbability();
			if (pathProb > maxProb) {
				maxProb = pathProb;
				maxSequence = path.getTagSequence();
			}
		}
		
		if (maxSequence == null)
			return new ArrayList<String>();
		
		Collections.reverse(maxSequence);
		
		return maxSequence;
	}
	
	/**
	 * Calculate best subpaths for the candidate tags from the specified word.
	 * 
	 * @param word
	 * @param previousPaths
	 * @param bestSubpaths
	 */
	private void calculateBestSubpaths(String word, Deque<Subpath> previousPaths, Map<String, 
			Map<String, Subpath>> bestSubpaths) {
		
		Set<String> candidateTags = model.getCandidateTagsForWord(word);
		
		for (String candidateTag : candidateTags) {
			for (Subpath previousPath : previousPaths) {
				
				Deque<String> contextUsed = new ArrayDeque<>();
				List<String> path = previousPath.getTagSequence();
				
				Double logProb = model.getLogProbabilityAndContextUsed(candidateTag, word, path, contextUsed);
				Double pathProb = previousPath.getLogProbability();

				logProb += pathProb;

				if (bestSubpaths.containsKey(candidateTag)) {
					
					Map<String, Subpath> subpath = bestSubpaths.get(candidateTag);
					
					if (!subpath.containsKey(contextUsed.toString()) || (logProb >= subpath.get(contextUsed.toString()).getLogProbability())) {
						subpath.put(contextUsed.toString(), new Subpath(path, logProb));
					}

				} else {
					Map<String, Subpath> subpath = new THashMap<String, Subpath>();
					subpath.put(contextUsed.toString(), new Subpath(path, logProb));
					bestSubpaths.put(candidateTag, subpath);
				}
			}
		}
	}
}
