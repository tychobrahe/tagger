package br.unicamp.iel.tycho.tagger.vlmm.model;

import gnu.trove.map.hash.THashMap;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.annotations.Expose;

public class Node implements Serializable {

    private static final long serialVersionUID = 5197643077728427544L;

    @Expose
    private int depth;
    
    @Expose
    private Double count = 0.0;
    
    @Expose
    private boolean finalLeaf;
    
    private Node parent;
    
    @Expose
    private String name;
    
    @Expose
    private Map<String, Node> branches;
    
    @Expose
    private Map<String, Sprout> sprouts;

    public Node() {
        depth = -1;
        parent = null;
        finalLeaf = false;
        branches = new THashMap<String, Node>();
        sprouts = new THashMap<String, Sprout>();
    }

    public Node(double count, int depth, boolean finalLeaf) {
        this();
        this.depth = depth;
        this.count = count;
        this.finalLeaf = finalLeaf;
    }
    
    /**
     * Creates a new node that is a copy of the given node.
     *  
     * @param node The node to be copied.
     */
    public Node(Node node) {
        this();
        setDepth(node.getDepth());
        setCount(node.getCount());
        setFinalLeaf(node.isFinalLeaf());
        setName(node.getName());
        setParent(node.getParent());
        this.branches.putAll(node.getBranches());
        this.sprouts.putAll(node.getSprouts());
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public double getCount() {
        return count;
    }

    public void setCount(double count) {
        this.count = count;
    }

    public boolean isFinalLeaf() {
        return finalLeaf;
    }

    public void setFinalLeaf(boolean is_final_leaf) {
        this.finalLeaf = is_final_leaf;
    }

    public Map<String, Node> getBranches() {
        return branches;
    }

    public void setBranches(Map<String, Node> branches) {
        this.branches = branches;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public Map<String, Sprout> getSprouts() {
        return sprouts;
    }

    public void setSprouts(Map<String, Sprout> sprouts) {
        this.sprouts = sprouts;
    }

    public void incrementCount() {
        this.count++;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Adds a branch tree to the node.
     */
    public void addBranch(String key, Node value) {
        if (this.getBranches() == null) {
            this.setBranches(new HashMap<String, Node>());
        }

        this.getBranches().put(key, value);
    }

    /**
     * Adds a sprout to the node.
     */
    public void addSprout(String key, Double probability) {

        if (this.getSprouts() == null) {
            this.setSprouts(new HashMap<String, Sprout>());
        }

        this.getSprouts().put(key, new Sprout(probability));
    }

    /**
     * Gets the sprout count by its name.
     * 
     * @param sproutName
     * @return
     */
    public Double getSproutCount(String sproutName) {
        if (this.getSprouts() == null)
            return 0.0;

        if (!this.getSprouts().containsKey(sproutName))
            return 0.0;

        return this.getSprouts().get(sproutName).getCount();
    }

    public String toString() {
        return "(" + getName() + "; depth: " + getDepth() + "; count: " + getCount() + "; parent: "
                + (getParent() != null ? getParent().getName() : "null") + "; number of sprouts: " + getSprouts().size() + "; number of branches: "
                + getBranches().size() + ")";
    }
}
