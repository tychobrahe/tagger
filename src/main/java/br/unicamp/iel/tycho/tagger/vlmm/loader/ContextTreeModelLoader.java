package br.unicamp.iel.tycho.tagger.vlmm.loader;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import br.unicamp.iel.tycho.tagger.vlmm.model.ContextTree;
import br.unicamp.iel.tycho.tagger.vlmm.model.Node;
import br.unicamp.iel.tycho.tagger.vlmm.util.NodeImportWrapper;

/**
 * Loads the context tree from a file.
 * Data is put at each line delimited by ; in the following format:
 * 
 * 0:45848:265:ADJ,1.1;
 * depth:probability:branches.size():branch1name,b1number;branch2name,b2number;:sprout1name,probability;sprout2name,probability;:
 *
 * @author Luiz Veronesi
 * @version 1.0 26/06/2013
 */
public class ContextTreeModelLoader {

	public ContextTree execute(String filename) throws Exception {

		List<String> lines = FileUtils.readLines(new File(filename), "UTF-8");
		
		List<ContextTreeLine> contextLines = this.readContextTreeLines(lines);
		
		Map<String, NodeImportWrapper> mapNodes = this.convertAffixLines(contextLines);
		
		Map<String, String> mapNodeNames = new HashMap<>();
		
    	Node root = null;
    	for (Map.Entry<String, NodeImportWrapper> entry : mapNodes.entrySet()) {

    		NodeImportWrapper nodeWrapper = entry.getValue();
    		Node node = nodeWrapper.getNode();
    		
    		if (mapNodeNames.containsKey(entry.getKey())) {
    			node.setName(mapNodeNames.get(entry.getKey()));
    		} else {
    			node.setName(entry.getKey());
    		}
    		
    		for (Map.Entry<String, String> branch : nodeWrapper.getBranches().entrySet()) {
    			node.addBranch(branch.getKey(), mapNodes.get(branch.getValue()).getNode());
    			
    			mapNodeNames.put(branch.getValue(), branch.getKey());
    		}
    		
    		// ROOT
    		if (entry.getKey().equals("0")) {
    			root = node;
    		}
    	}

    	return new ContextTree(root);
	}
	
    /**
     * Convert a list of context tree lines to a map wrapping the nodes.
     * 
     * @param affixLines
     * @return Map<String, NodeImportWrapper>
     */
    private Map<String, NodeImportWrapper> convertAffixLines(List<ContextTreeLine> contextLines) throws Exception {

    	// Ordering by the key it is easier to make the tree
        Map<String, NodeImportWrapper> nodes = new TreeMap<>();

        for (int i = 0; i < contextLines.size(); i++) {
        	
        	ContextTreeLine line = (ContextTreeLine) contextLines.get(i);
        	
        	Integer depth = 0;
        	
        	// split depth from level order, but level order is not important at this moment
        	String[] depthSplit = line.getDepthOrder().split(ContextTreeLine.DEPTH_DELIM);
        	if (depthSplit.length == 2) {
        		depth = new Integer(depthSplit[0]);
        	}

        	Node node = new Node(line.getProbability(), depth, false);
			
        	Map<String, String> mapBranches = new HashMap<>();
	    	StringTokenizer branches = new StringTokenizer(line.getBranches(), ContextTreeLine.BRANCHES_DELIM);
	    	while (branches.hasMoreTokens()) {
	    	    
	    		/*
	    		 * Splits this branch into 2: first element is the first character of the node at branch, second element 
	    		 * is the combination of depth and level order for this node.
	    		 */
	    		String[] branchData = branches.nextToken().split(ContextTreeLine.BRANCH_DATA_DELIM);

	    		if (branchData.length != 2) {
	    			throw new Exception("ERROR[5]: affix tree file on line " + i + ": invalid line format");
	    	    }

	    		mapBranches.put(branchData[0], branchData[1]);
	    	}

	    	if (!StringUtils.isEmpty(line.getSprouts())) {

		    	StringTokenizer sprouts = new StringTokenizer(line.getSprouts(), ContextTreeLine.SPROUT_DELIM);
		    	
		    	while (sprouts.hasMoreTokens()) {
		    	
		    		String sprout = sprouts.nextToken();

		    		// P,0.0576923076923077
		    		String[] innerSproutData = sprout.split(ContextTreeLine.SPROUT_DATA_DELIM);
		    		if (innerSproutData.length != 2) {
		    			throw new Exception("ERROR[6]: affix tree file on line " + i + ": invalid line format");
		    	    }
		    	    
		    		node.addSprout(innerSproutData[0], new Double(innerSproutData[1]));
		    	}
	    	}
	    	
	    	nodes.put(line.getDepthOrder(), new NodeImportWrapper(line.getDepthOrder(), node, mapBranches));
        }
        
        return nodes;
    }

	/**
	 * Read context tree lines.
	 * 
	 * @param lines
	 * @return List<ContextTreeLine>
	 */
	private List<ContextTreeLine> readContextTreeLines(List<String> lines) throws Exception {
		
		List<ContextTreeLine> contextLines = new ArrayList<>();
	    for (int i = 0; i < lines.size(); i++) {
	    	
	    	String line = lines.get(i);
	    	
	    	String[] lineTokens = line.split(ContextTreeLine.LINE_DELIM, -1);
	        if (lineTokens.length != 7) {
	        	throw new Exception("ERROR[1]: context tree file on line " + i + ": invalid line format");
	        }

	        // First element is depth.levelOrder
	        ContextTreeLine contextLine = new ContextTreeLine();

	        contextLine.setDepthOrder(lineTokens[0]);
	    	if (!contextLine.getDepthOrder().equals("0") && contextLine.getDepthOrder().split(ContextTreeLine.DEPTH_DELIM).length != 2) {
	    		throw new Exception("ERROR[2]: affix tree file on line " + i + ": invalid line format.");
	    	}

	    	contextLine.setProbability(new Double(lineTokens[1]));
	    	contextLine.setNumBranches(new Integer(lineTokens[2]));
	    	contextLine.setBranches(lineTokens[3]);

            if (!StringUtils.isEmpty(contextLine.getBranches())) {
		    	if (!contextLine.getNumBranches().equals(contextLine.getBranches().split(ContextTreeLine.BRANCHES_DELIM).length)) {
		    		throw new Exception("ERROR[4]: affix tree file on line " + i + ": invalid line format.");
		    	}
            }
            
            contextLine.setSprouts(lineTokens[4]);
            
            contextLines.add(contextLine);
	    }
	    
	    return contextLines;
	}
	
	/**
     * Class used for reading ContextTree file lines and make conversion easier.
     *
     * @author Luiz Veronesi
     * @version 1.0 24/06/2013
     */
    private class ContextTreeLine {
    	
        public static final String LINE_DELIM = ":";
        public static final String DEPTH_DELIM = "\\.";
        public static final String BRANCHES_DELIM = ";";
        public static final String BRANCH_DATA_DELIM = ",";
        public static final String SPROUT_DELIM = ";";
        public static final String SPROUT_DATA_DELIM = ",";
    	
        /**
         * A branch number is its depth dot its order of appearaence at this depth (format: #.#)
         */
        private String depthOrder;
        
        private Double probability;
        
        private Integer numBranches;
        
        private String branches;
        
        private String sprouts;

		public String getDepthOrder() {
			return depthOrder;
		}

		public void setDepthOrder(String depthOrder) {
			this.depthOrder = depthOrder;
		}

		public Double getProbability() {
			return probability;
		}

		public void setProbability(Double probability) {
			this.probability = probability;
		}

		public Integer getNumBranches() {
			return numBranches;
		}

		public void setNumBranches(Integer numBranches) {
			this.numBranches = numBranches;
		}

		public String getBranches() {
			return branches;
		}

		public void setBranches(String branches) {
			this.branches = branches;
		}

		public String getSprouts() {
			return sprouts;
		}

		public void setSprouts(String sprouts) {
			this.sprouts = sprouts;
		}
    }
}
