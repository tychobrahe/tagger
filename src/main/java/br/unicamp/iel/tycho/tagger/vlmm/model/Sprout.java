package br.unicamp.iel.tycho.tagger.vlmm.model;

import java.io.Serializable;

/**
 * 
 * @author kepler
 * @version 3.0 2013
 * 
 * Using only counts instead of probabilities.
 */
public class Sprout implements Serializable {

	private static final long serialVersionUID = 7127132778759068869L;
	
	private Double count = 0.0;

	public Sprout() {
	}

	public Sprout(Double initialCount) {
		setCount(initialCount);
	}

	public Double getCount() {
		return count;
	}

	public void setCount(Double count) {
		this.count = count;
	}

	public void incrementCount() {
		this.count++;
	}
}
