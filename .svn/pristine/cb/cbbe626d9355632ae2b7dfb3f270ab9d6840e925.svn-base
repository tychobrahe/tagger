package br.usp.ime.vlmm.util;

import java.io.File;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.ArrayUtils;

import br.usp.ime.vlmm.automaton.StringCounter;

public class Functions {

	public static final String ENDL = "\n";
	public static final String[] PUNCTUATION_SYMBOLS = {".", ",", "("};
	public static final String[] PUNCTUATION_NAMES = {"FPUNC", "COMMA", "PARENTHESES"};

	public static final String[] FINAL_PUNCTUATION_SYMBOLS = {".", "?", "!", ";", ":", "..."};
	
	public static final String UNTAGGED_TEXT_DELIM = " ";

	/**
	 * Read untagged text from file.
	 * 
	 * @param fileName file name
	 * @return List<String> returns words from the file
	 * 
	 * @author F�bio Kepler
	 * @version 1.0
	 * 
	 * @author Luiz Veronesi
	 * @version 2.0 14/09/2012
	 * Method signature modified. Exceptions are thrown in case of error and a boolean value is no
	 * longer returned. 
	 */
	public static List<String> readUntaggedTextFromFile(String fileName) throws Exception {
		
		File file = new File(fileName);
		
		if (!file.canRead()) {
			throw new Exception("Error: could not open file '" + fileName + "' for reading");
	    }

		List<String> lines = FileUtils.readLines(file);
		
		return getWordsFromUntaggedText(lines);
	}

	public static List<String> getWordsFromUntaggedText(String... lines) {
		List<String> list = new ArrayList<String>();
		Collections.addAll(list, lines);
		return getWordsFromUntaggedText(list);
	}
	
	/**
	 * Retrieve words from an untagged collection of words.
	 * 
	 * @param lines
	 * @return List<String>
	 */
	public static List<String> getWordsFromUntaggedText(List<String> lines) {
		
		Pattern r = Pattern.compile("\\p{Punct}", Pattern.CASE_INSENSITIVE);
		List<String> words = new ArrayList<String>();
		
		for (String line : lines) {
			
			String[] line_tokens = line.split(UNTAGGED_TEXT_DELIM);
			
			for (String token : line_tokens) {
				Matcher m = r.matcher(token);
				
			    if (m.find()) {
		    		String foundPunct = m.group();
		    		StringTokenizer st = new StringTokenizer(token, foundPunct);
		    		words.add(st.nextToken());
		    		words.add(foundPunct);
			    } else {
			    	words.add(token);
			    }
			}
		}
		
		return words;
	}
	
	/**
	 * Retira do conjunto os valores ',', '.', '(' e inclui seus respectivos nomes: "COMMA", "FPUNC" e "PARENTHESES".
	 */
	public static void namePunctuation(List<String> tags) {
		changePunctuation(tags, true);
	}

	public static void revertPunctuation(List<String> tags) {
		
		changePunctuation(tags, false);
		
	    /*String old_tag, new_tag;
	    old_tag = "COMMA";
	    new_tag = ",";
	    replace(tags.begin(), tags.end(), old_tag, new_tag);
	    old_tag = "FPUNC";
	    new_tag = ".";
	    replace(tags.begin(), tags.end(), old_tag, new_tag);
	    old_tag = "PARENTHESES";
	    new_tag = "(";
	    replace(tags.begin(), tags.end(), old_tag, new_tag);*/
	}

	/**
	 * Altera as pontua��es para seus nomes e vice-versa.
	 * 
	 * @param tags
	 * @param order true altera de pontua��o para nome e false o inverso.
	 */
	private static void changePunctuation(List<String> tags, boolean order) {
		
		String[] first = PUNCTUATION_SYMBOLS;
		String[] second = PUNCTUATION_NAMES;
		
		if (order) {
			first = PUNCTUATION_NAMES;
			second = PUNCTUATION_SYMBOLS;
		}
		
		for (int i = 0; i < first.length; i++) {
			Collections.replaceAll(tags, first[i], second[i]);
		}
		
	}

	/*
	 * Read tagged text in the format: word1/tag1 word2/tag2.
	 */
	//bool ReadTaggedTextFromFile(String file_name, String delimiter, vector<String> &words, vector<String> &tags)
	public static boolean readTaggedTextFromFile(String file_name, String delimiter, List<String> words, List<String> tags) throws Exception {
		
	    //ifstream infile;
	    //infile.open(file_name.c_str());
		File file = new File(file_name);
		
	    //if (!infile.good()) {
		if (!file.canRead()) {
	        //cerr << "Error: could not open file '" << file_name << "' for reading" << endl;
			System.err.println("Error: could not open file '" + file_name + "' for reading");
	        return false;
	    }

	    //String line;
	    //String delim;
	    //vector<String> line_tokens;
	    int line_number = 0;

	    //delim = delimiter;
	    String delim = delimiter;
		
	    List<String> lines = FileUtils.readLines(file);
	    
	    //while (true) {
	    for (String line : lines) {
	    
	        line_number++;
	        //infile >> line;
	        //if (infile.eof()) break;

	        //line_tokens = StringTokenize(line, " ");
	        String[] line_tokens = line.split(" ");

	        //vector<String> word_tag; // loop variable
	        //for (int i = 0; i < (int)line_tokens.size(); i++) {
	        for (String line_token : line_tokens) {
	            //word_tag = StringTokenize(line_tokens[i], delim);
	        	String[] word_tag = line_token.split(delim);
	        	
	            //if (word_tag.size() != 2) {
	        	if (word_tag.length != 2) {
	        		
	        		if (delim == "_") {
	        		    
	        			//if (word_tag.size() == 3) {
	        			if (word_tag.length == 3) {
	            			if (word_tag[0].length() == 0) {
	            			    word_tag[0] = delim;
	            			} else {
	            			    word_tag[0] += word_tag[1];
	            			}
	            			
	            			if (word_tag[2].length() != 0) {
	            			    word_tag[1] = word_tag[2];
	            			}
	            			
	                        //word_tag.pop_back();
	            			ArrayUtils.remove(word_tag, word_tag.length - 1);
	            			
	        		    } else {
	        		        //cerr << "Warning: reading file '" << file_name << "': in line '" << line_tokens[i] << "': missing a word or tag. Proceeding." << endl;
	        		    	System.err.println("Warning: reading file '" + file_name + "': in line '" + line_token + "': missing a word or tag. Proceeding.");
	                        continue;
	                    }
	        		} else {
	        		    //cerr << "Warning: reading file '" << file_name << "': in line '" << line_tokens[i] << "': missing a word or tag. Proceeding." << endl;
	        			System.err.println("Warning: reading file '" + file_name + "': in line '" + line_token + "': missing a word or tag. Proceeding.");
	        		    continue;
	        		}
	            }
	        	
	    	    if (word_tag[0].length() == 0) {
	    	    	
	    		    /*cerr << "Warning: reading file '" << file_name
	        			 << "': in line[" << line_number << "] '" << line_tokens[i]
	                     << "'" << endl
	        			 << "         Word has length zero. Proceeding." << endl;
	    		    cerr << "         (The line is: '" << line << "')" << endl;*/
	    		    
	    	    	StringBuffer sb = new StringBuffer();
	    	    	sb.append("Warning: reading file '" + file_name);
	    	    	sb.append("': in line[" + line_number + "] '" + line_token);
	    	    	sb.append("'" + ENDL);
	    	    	sb.append("         Word has length zero. Proceeding." + ENDL);
	    	    	sb.append("         (The line is: '" + line + "')" + ENDL);

	    		    continue;
	    	    }
	    	    
	            //words.push_back(word_tag[0]);
	    	    words.add(word_tag[0]);
	    	    
	            //tags.push_back(word_tag[1]);
	    	    tags.add(word_tag[1]);
	        }
	    }
	    
	    //ChangeConflictingTags(tags);
	    namePunctuation(tags);

	    //infile.close();
	    return true;
	}

	/**
	 * Recupera os tokens de acordo com o delimitador fornecido.
	 * 
	 * @param source
	 * @param delimiter
	 * @return List<String>
	 */
	public List<String> stringTokenize(String source, String delimiter) {

		StringTokenizer st = new StringTokenizer(source, delimiter);
	    List<String> tokens = new ArrayList<String>();
	    
	    while (st.hasMoreTokens()) {
	    	tokens.add(st.nextToken());
	    }
	    
	    return tokens;
	}

	/**
	 * Cria uma lista contendo listas de Strings sendo que cada lista cont�m todos os
	 * elementos at� o delimitador informado.
	 * 
	 * @param source
	 * @param delimiter
	 * @return List<List<String>>
	 */
	public static List<List<String>> vectorStringTokenize(List<String> source, String[] delimiter) {
		
		List<List<String>> vectorTokens = new ArrayList<List<String>>();
		List<String> subvector = new ArrayList<String>();

		for (String str : source) {
			if (ArrayUtils.contains(delimiter, str)) {
				subvector.add(str);
				vectorTokens.add(subvector);
				subvector = new ArrayList<String>();
			} else {
				subvector.add(str);
			}
		}

	    return vectorTokens;
	}

	/**
	 * Cria uma lista contendo listas de Strings sendo que cada lista cont�m todos os
	 * elementos at� um dos delimitadores informados.
	 *  
	 * @param source
	 * @param delimiters
	 * @return
	 */
	List<List<String>> vectorStringTokenize(List<String> source, Set<String> delimiters) {

		List<List<String>> vectorTokens = new ArrayList<List<String>>();
		List<String> subvector = new ArrayList<String>();

		for (String String : source) {
			if (!delimiters.contains(String)) {
				subvector.add(String);
				vectorTokens.add(subvector);
				subvector = new ArrayList<String>();
			} else {
				subvector.add(String);
			}
		}

	    return vectorTokens;
	}

	public static List<List<String>> imitateVectorStringTokenize(List<String> source, List<List<String>> models) {
		
	    //vector< vector<String> > vector_tokens;
	    //vector<String>::iterator it_source; // loop variable
		List<List<String>> vector_tokens = new ArrayList<List<String>>();
		
	    //it_source = source.begin();
	    //for (unsigned int i = 0; i < model.size(); i++) {
		
		int sourcePos = 0;
		
	    for (List<String> model : models) {
	    
	        //if (it_source+model[i].size() > source.end()) {
	    	if (sourcePos + model.size() > source.size()) {
	            //cerr << "Warning: in function ImitateVectorStringTokenize:" << endl
	            //     << "\t Operation incomplete: the source is shorter than the model" << endl;
	    		
	            System.err.println("Warning: in function ImitateVectorStringTokenize:");
	            System.err.println("\t Operation incomplete: the source is shorter than the model");
	            
	            return vector_tokens;
	        }
	    	
	        //vector<String> subvector(it_source, it_source+model[i].size());
	    	//vector_tokens.push_back(subvector);
	    	vector_tokens.add(source.subList(sourcePos, sourcePos + model.size()));
	        
	        //it_source += (int)model[i].size();
	    	sourcePos += model.size();
	    }

	    return vector_tokens;
	}

	public static boolean writeTaggedTextToFile(String file_name, String delim, List<List<String>> words, List<List<String>> tags) throws Exception {
		
	    //ofstream outfile;
	    //outfile.open(file_name.c_str());

		File file = new File(file_name);
		
	    //if (!outfile.good()) {
		if (!file.canWrite()) {
	        //cerr << "Error: could not open file '" << file_name << "' for writing" << endl;
			System.err.println("Error: could not open file '" + file_name + "' for writing");
	        return false;
	    }

	    if (words.size() != tags.size()) {
	        //cerr << "Error: writing tagged text: number of words not equal number of tags. Aborting." << endl;
	        System.err.println("Error: writing tagged text: number of words not equal number of tags. Aborting.");
	        return false;
	    }

	    StringBuffer sb = new StringBuffer();
	    
	    for (int i = 0; i < words.size(); i++) {
	        if (words.get(i).size() != tags.get(i).size()) {
		        //cerr << "Error: writing tagged text: number of words not equal number of tags. Aborting." << endl;
		        System.err.println("Error: writing tagged text: number of words not equal number of tags. Aborting.");
	            return false;
	        }
	        
	        for (int j = 0; j < words.get(i).size(); j++) {
	            //outfile << words[i][j] << delim << tags[i][j] << " ";
	            sb.append(words.get(i).get(j) + delim + tags.get(i).get(j) + " ");
	        }
	        
		    //outfile << endl;
		    sb.append(ENDL);
	    }

	    //outfile.close();
	    FileUtils.write(file, sb);
	    
	    return true;
	}

	public static boolean writeTaggedTextToFileSimple(String file_name, String delim, List<String> words, List<String> tags) throws Exception {
		
	    //ofstream outfile;
	    //outfile.open(file_name.c_str());

		File file = new File(file_name);
		
	    //if (!outfile.good()) {
		//if (!file.canWrite()) {
	        //cerr << "Error: could not open file '" << file_name << "' for writing" << endl;
			//System.err.println("Error: could not open file '" + file_name + "' for writing");
	        //return false;
	    //}

	    if (words.size() != tags.size()) {
	        //cerr << "Error: writing tagged text: number of words not equal number of tags. Aborting." << endl;
	        throw new Exception("Error: writing tagged text: number of words not equal number of tags. Aborting.");
	    }

	    StringBuffer sb = new StringBuffer();
	    
	    for (int i = 0; i < words.size(); i++) {
	        //outfile << words[i] << delim << tags[i] << " ";
	    	sb.append(words.get(i) + delim + tags.get(i) + " ");
		    
	    	if (tags.get(i).equals(".")) {
	    		//outfile << endl;
	    		sb.append(ENDL);
	    	}
	    }

	    //outfile.close();
	    FileUtils.write(file, sb);
	    
	    return true;
	}


	// model_tags are the correct tags, while test_tags are the tags obtained by the Viterbi algorithm on tagging words.
	//double CalculateAccuracy(vector<String> model_tags, vector<String> test_tags, vector<String> words, set<String> known_words, set<String> ambiguous_words, set<String> final_punctuation_symbols)
	public static double calculateAccuracy(List<String> model_tags, List<String> test_tags, List<String> words, 
			Set<String> known_words, Set<String> ambiguous_words, String[] final_punctuation_symbols) throws Exception {
		
	    int num_correct = 0;
	    int num_wrong = 0;
	    int num_unknown_words_correct = 0;
	    int num_unknown_words_wrong = 0;
	    int num_unknown_words = 0;
	    int num_ambiguous_words_correct = 0;
	    int num_ambiguous_words_wrong = 0;
	    int line_number = 1;
	    int last_fpunc = 0;
	    
	    //vector<int> unknown_words_index;
	    List<Integer> unknown_words_index = new ArrayList<Integer>();
	    
	    int num_words = words.size();

	    //NEW! 2005/05/19
	    //map<String, double> most_wrong_ambiguous_words;
	    StringCounter most_wrong_ambiguous_words = new StringCounter();
	    
	    //cout << "Model tags: "<<model_tags.size()<<" Test tags: "<<test_tags.size()<<" words: "<<words.size()<<" known words: " << known_words.size()<<endl;
	    System.out.println("Model tags: " + model_tags.size() + " Test tags: " + test_tags.size() + " words: " + words.size() + " known words: " + known_words.size());
	    
	    //for (int i = 0; i < num_words; i++) {
	    for (int i = 0; i < words.size(); i++) {

	        //if (known_words.find(words[i]) == known_words.end()) { //so words[i] is unknown
	    	if (!known_words.contains(words.get(i))) {
	    		
	            //unknown_words_index.push_back(i);
	    		unknown_words_index.add(i);
	            num_unknown_words++;
	        }
	    }
	    
	    int cur_index = -1;
	    if (!unknown_words_index.isEmpty()) {
	        cur_index = 0;
	    }

	    String file_name = "errors.info";
	    File outfile = new File(file_name);
	    
	    //ofstream outfile;
	    //outfile.open(file_name.c_str());
	    
	    //if (!outfile.good()) {
	    if (!outfile.canWrite()) {
	        //cerr << "Warning: could not open file '" << file_name << "' for writing tagging errors." << endl;
	    	System.err.println("Warning: could not open file '" + file_name + "' for writing tagging errors.");
	    }
	    
	    // para escrever o arquivo
	    StringBuffer sb = new StringBuffer();
	    
	    //outfile << "CONTEXT WORD/WRONG_TAG%CORRECT_TAG CONTEXT" << endl;
	    sb.append("CONTEXT WORD/WRONG_TAG%CORRECT_TAG CONTEXT" + ENDL);
	    
	    //int min_size = min(model_tags.size(), test_tags.size());
	    int min_size = model_tags.size();
	    if (test_tags.size() < model_tags.size()) {
	    	min_size = test_tags.size();
	    }
	    
	    for (int i = 0; i < min_size; i++) {
	    	
	        //if (model_tags[i] == test_tags[i]) {
	    	if (model_tags.get(i).equals(test_tags.get(i))) {
	    		
	            num_correct++;

	            //if (final_punctuation_symbols.find(words[i]) != final_punctuation_symbols.end()) {
	            if (ArrayUtils.contains(final_punctuation_symbols, words.get(i))) {
	                line_number++;
	            	last_fpunc = i;
	    	    }
	            
	            //if (cur_index >= 0 && i == unknown_words_index[cur_index]) {
	            if (cur_index >= 0 && i == unknown_words_index.get(cur_index)) {
	                num_unknown_words_correct++;
	                cur_index++;
	            }
	            
	            //if (ambiguous_words.find(words[i]) != ambiguous_words.end()) { // So current word is ambiguous.
	            if (ambiguous_words.contains(words.get(i))) {
	                num_ambiguous_words_correct++;
	            }
	            
	        } else {
	        	
	            String know_word = "word(!ambiguous)";
	            num_wrong++;
	            
	            //if (ambiguous_words.find(words[i]) != ambiguous_words.end()) { // So current word is ambiguous.
	            if (ambiguous_words.contains(words.get(i))) {
	                num_ambiguous_words_wrong++;
	                know_word = "word";
	                //most_wrong_ambiguous_words[words[i]]++;
	                most_wrong_ambiguous_words.put(words.get(i));
	            }
	            
	    	    //if (cur_index >= 0 && i == unknown_words_index[cur_index]) {
	            if (cur_index >= 0 && i == unknown_words_index.get(cur_index)) {
	                num_unknown_words_wrong++;
	                cur_index++;
	                know_word = "word(U)";
	            }
	            
	    	    /*outfile << "On sentence " << line_number << " "<<know_word<<" " << i - last_fpunc << ": ";
	            outfile << ((i > 1) ? (words[i-2]+"/"+test_tags[i-2]) : "") << " ";
	            outfile << ((i > 0) ? (words[i-1]+"/"+test_tags[i-1]) : "") << " ";
	    	    outfile << words[i]+"/"+test_tags[i]+"%"+model_tags[i] << " ";
	    	    outfile << ((i+1 < min_size) ? (words[i+1]+"/"+test_tags[i+1]) : "") << endl;*/
	    	    
	    	    sb.append("On sentence " + line_number + " " + know_word + " " + (i - last_fpunc) + ": ");
	    	    sb.append(((i > 1) ? (words.get(i-2) + "/" + test_tags.get(i-2)) : "") + " ");
	    	    sb.append(((i > 0) ? (words.get(i-1) + "/" + test_tags.get(i-1)) : "") + " ");
	    	    sb.append(words.get(i) + "/" + test_tags.get(i) + "%" + model_tags.get(i) + " ");
	    	    sb.append(((i+1 < min_size) ? (words.get(i+1) + "/" + test_tags.get(i+1)) : "") + ENDL);

	        }
	    }
	    
	    //printf("[Stats of tagging]\n");
	    System.out.println("[Stats of tagging]");
	    
	    //printf("\tMost wrong tagged words:\n");
	    System.out.println("\tMost wrong tagged words:");
	    
	    //map<String, double>::iterator it_most; // loop variable
	    
	    //multimap<double, String> more;
	    // Preferi adotar uma lista de Map.Entry<Double, String>
	    List<Map.Entry<Double, String>> more = new ArrayList<Map.Entry<Double, String>>();
	    
	    //more.insert(pair<double,String>(0.0,""));
	    more.add(new AbstractMap.SimpleEntry<Double, String>(0.0, ""));
	    
	    //for (it_most = most_wrong_ambiguous_words.begin(); it_most != most_wrong_ambiguous_words.end(); it_most++) {
	    for (Map.Entry<String, Double> it_most : most_wrong_ambiguous_words.entrySet()) {
	    	
	    	// Primeiro elemento de more
	    	Map.Entry<Double, String> first = more.iterator().next();
	    	
	        //if ((*it_most).second > (*more.begin()).first) {
	    	if (it_most.getValue() > first.getKey()) {
	    	
	            if (more.size() < 10) {
	            	// more.insert(pair<double,String>((*it_most).second, (*it_most).first));
	            	more.add(new AbstractMap.SimpleEntry<Double, String>(it_most.getValue(), it_most.getKey()));
	            } else {
	                
	            	//more.erase(more.begin());
	            	more.remove(first);
	                
	                //more.insert(pair<double,String>((*it_most).second, (*it_most).first));
	                more.add(new AbstractMap.SimpleEntry<Double, String>(it_most.getValue(), it_most.getKey()));
	            }
	        }
	    }
	    
	    //multimap<double,String>::reverse_iterator it_more; // loop variable
	    //for (it_more = more.rbegin(); it_more != more.rend(); it_more++) {
	    for (Map.Entry<Double, String> it_more : more) {
	        //printf("\t\t%s: %g\n", (*it_more).second.c_str(), (*it_more).first);
	    	System.out.println("\t\t" + it_more.getValue() + ":" + it_more.getKey() + ENDL);
	    }
	        
	    int num_known_words = num_words - num_unknown_words;
	    int num_known_words_correct = num_correct - num_unknown_words_correct;
	    int num_known_words_wrong = num_wrong - num_unknown_words_wrong;
	    double percent_known_words = (double)num_known_words / (double)num_words;
	    double known_words_accuracy = (double)((double)num_known_words_correct / (double)num_known_words);
	    
	    //printf("Known words: %d (%g%%) Correct tagged: %d Wrong tagged: %d Accuracy: %g%%\n",
	    //        num_known_words, 100*percent_known_words, num_known_words_correct, num_known_words_wrong, known_words_accuracy*100);

	    FileUtils.write(outfile, sb);
	    
	    sb = new StringBuffer();
	    sb.append("Known words: " + num_known_words + " (" + 100*percent_known_words + "%)");
	    sb.append(" Correct tagged: " + num_known_words_correct);
	    sb.append(" Wrong tagged: " + num_known_words_wrong);
	    sb.append(" Accuracy: " + known_words_accuracy*100 + "%");
	    System.out.println(sb.toString());
	    
	    
	    if (num_unknown_words != 0) {
	        double percent_unknown_words = (double)num_unknown_words / (double)num_words;
	        double unknown_words_accuracy = (double)((double)num_unknown_words_correct / (double)num_unknown_words);
	        
	        //printf("Unknown words: %d (%g%%) Correct tagged: %d Wrong tagged: %d Accuracy: %g%%\n",
	        //        num_unknown_words, 100*percent_unknown_words, num_unknown_words_correct, num_unknown_words_wrong, unknown_words_accuracy*100);
	        
		    sb = new StringBuffer();
		    sb.append("Unknown words: " + num_unknown_words + " (" + 100*percent_unknown_words + "%)");
		    sb.append(" Correct tagged: " + num_unknown_words_correct);
		    sb.append(" Wrong tagged: " + num_unknown_words_wrong);
		    sb.append(" Accuracy: " + unknown_words_accuracy*100 + "%");
		    System.out.println(sb.toString());
	    }
	    
	    double num_ambiguous_words = (double)(num_ambiguous_words_correct + num_ambiguous_words_wrong);
	    double percent_ambiguous_words = num_ambiguous_words / (double)num_words;
	    double ambiguous_words_accuracy = (double)((double)num_ambiguous_words_correct / num_ambiguous_words);
	    
	    //printf("Ambiguous words: %g (%g%%) Correct tagged: %d Wrong tagged: %d Accuracy: %g%%\n",
	    //        num_ambiguous_words, 100*percent_ambiguous_words, num_ambiguous_words_correct, num_ambiguous_words_wrong, ambiguous_words_accuracy*100);

	    sb = new StringBuffer();
	    sb.append("Ambiguous words: " + num_ambiguous_words + " (" + 100*percent_ambiguous_words + "%)");
	    sb.append(" Correct tagged: " + num_ambiguous_words_correct);
	    sb.append(" Wrong tagged: " + num_ambiguous_words_wrong);
	    sb.append(" Accuracy: " + ambiguous_words_accuracy*100 + "%");
	    System.out.println(sb.toString());
	    
	    //printf("Total correct tags: %d Total wrong tags: %d\n", num_correct, num_wrong);
	    System.out.println("Total correct tags: " + num_correct + " Total wrong tags: " + num_wrong);

	    //outfile.close();
	    
	    double accuracy = (double)num_correct / (double)model_tags.size();
	    return accuracy;
	}

	/*bool WriteTextToFile(String file_name, String headline, vector<String> text, String vector_separator = "\n")
	{
	    ofstream outfile;
	    outfile.open(file_name.c_str());

	    if (!outfile.good()) {
	        cerr << "Error: could not open file '" << file_name << "' for writing" << endl;
	        return false;
	    }

	    outfile << headline;
	    for (int i = 0; i < (int)text.size(); i++) {
	        outfile << text[i] << vector_separator;
	    }

	    outfile.close();
	    return true;
	}*/
}
